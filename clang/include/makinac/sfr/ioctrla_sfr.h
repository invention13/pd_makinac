/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file ioctrla_sfr.h
 */

#ifndef __IOCTRLA_SFR_H__
#define __IOCTRLA_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for IOCTRLA.
 */
typedef struct {

  uint8_t  BLOCK_SEL;                         /* +0x000 */
  uint8_t  _RESERVED_01[3];                   /* +0x001 */

  uint8_t  TEST_SEL0;                         /* +0x004 */
  uint8_t  _RESERVED_05[3];                   /* +0x005 */

  uint8_t  TEST_SEL1;                         /* +0x008 */
  uint8_t  _RESERVED_09[3];                   /* +0x009 */

  uint8_t  TEST_SEL2;                         /* +0x00C */
  uint8_t  _RESERVED_0D[3];                   /* +0x00D */

  uint8_t  TEST_SEL3;                         /* +0x010 */
  uint8_t  _RESERVED_11[3];                   /* +0x011 */

  uint8_t  TEST_SEL4;                         /* +0x014 */
  uint8_t  _RESERVED_15[3];                   /* +0x015 */

  uint8_t  TEST_SEL5;                         /* +0x018 */
  uint8_t  _RESERVED_19[3];                   /* +0x019 */

  uint8_t  TEST_SEL6;                         /* +0x01C */
  uint8_t  _RESERVED_1D[3];                   /* +0x01D */

  uint8_t  TEST_SEL7;                         /* +0x020 */
  uint8_t  _RESERVED_21[3];                   /* +0x021 */

} IOCTRLA_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of IOCTRLA SFRS.
 */
#define IOCTRLA_SFRS ((__IO IOCTRLA_SFRS_t *)0x50000800)

#endif /* end of __IOCTRLA_SFR_H__ section */


