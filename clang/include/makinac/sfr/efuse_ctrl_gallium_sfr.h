/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file efuse_ctrl_gallium_sfr.h
 */

#ifndef __EFUSE_CTRL_GALLIUM_SFR_H__
#define __EFUSE_CTRL_GALLIUM_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for EFUSE_CTRL_GALLIUM.
 */
typedef struct {

  uint8_t  PROG_ENA_WIDTH;                    /* +0x000 */
  uint8_t  _RESERVED_01[3];                   /* +0x001 */

  uint32_t DATA0;                             /* +0x004 */

  uint32_t DATA1;                             /* +0x008 */

} EFUSE_CTRL_GALLIUM_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of EFUSE_CTRL_GALLIUM SFRS.
 */
#define EFUSE_CTRL_GALLIUM_SFRS ((__IO EFUSE_CTRL_GALLIUM_SFRS_t *)0x50000c00)

#endif /* end of __EFUSE_CTRL_GALLIUM_SFR_H__ section */


