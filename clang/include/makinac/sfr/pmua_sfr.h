/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file pmua_sfr.h
 */

#ifndef __PMUA_SFR_H__
#define __PMUA_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for PMUA.
 */
typedef struct {

  union {
    struct {
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  FASTBOOT                 :  1; /*!< Fast boot */
      uint8_t  FASTSHUTDOWN             :  1; /*!< Fast shutdown */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  7; /*   (reserved) */
      uint8_t  ENA_CLK_SYS_GATING       :  1; /*!< Enable 'clock sys' gating */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CTRL; /* +0x000 */

  union {
    struct {
      uint8_t  ENABLE_BIAS              :  8; /*!< Enable bias dwell time */
      uint8_t  DISABLE_VCORE            :  8; /*!< Discharge vcore dwell time */
      uint8_t  SELECT_VRETAIN           :  4; /*!< Select vretain dwell time */
      uint8_t  REVERT_TO_VCORE          :  4; /*!< Revert to vcore dwell time */
      uint8_t  HYPERSLEEP_REQUEST       :  8; /*!< Hypersleep Request dwell time */
    };
    uint32_t WORD;
  } DWELL; /* +0x004 */

  uint8_t  HYPERSLEEP_ENA;                    /*<! HYPERSLEEP Enable +0x008 */
  uint8_t  _RESERVED_09[3];                   /* +0x009 */

  union {
    struct {
      uint8_t  IGNORE_VRETAIN           :  1; /*!< Ignore vretain requests */
      uint8_t  ENABLE_HYPERSLEEP_WITH_DAP :  1; /*!< Allows Hypersleep Power Mode with DAP connected */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  VCORE_FW_SEL             :  1; /*!< Give control of the 1.2V regulator enable to Firmware */
      uint8_t  VCORE_FW_ENA             :  1; /*!< Control the enable of the 1.2V regulator when VCORE_FW_SEL is set */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } DEBUG; /* +0x00C */

  uint8_t  STRB_SEL;                          /*<! Strobe Select +0x010 */
  uint8_t  _RESERVED_11[3];                   /* +0x011 */

  uint32_t _RESERVED_14[59];

  union {
    struct {
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  BUCK_OVERCUR             :  1; /*!< Interrupt Enable */
      uint8_t  BUCK_PGOODUP             :  1; /*!< Interrupt Enable */
      uint8_t  BUCK_PGOODDOWN           :  1; /*!< Interrupt Enable */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PMUA_IRQ_ENA; /* +0x100 */

  union {
    struct {
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  BUCK_OVERCUR             :  1; /*!< Flag Clear Register */
      uint8_t  BUCK_PGOODUP             :  1; /*!< Flag Clear Register */
      uint8_t  BUCK_PGOODDOWN           :  1; /*!< Flag Clear Register */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PMUA_IRQ_CLR; /* +0x104 */

  union {
    struct {
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  BUCK_OVERCUR             :  1; /*!< Interrupt Fired Flag Register */
      uint8_t  BUCK_PGOODUP             :  1; /*!< Interrupt Fired Flag Register */
      uint8_t  BUCK_PGOODDOWN           :  1; /*!< Interrupt Fired Flag Register */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PMUA_IRQ_FIRED; /* +0x108 */

  union {
    struct {
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  BUCK_OVERCUR             :  1; /*!< Interrupt Fired Masked Flag Register */
      uint8_t  BUCK_PGOODUP             :  1; /*!< Interrupt Fired Masked Flag Register */
      uint8_t  BUCK_PGOODDOWN           :  1; /*!< Interrupt Fired Masked Flag Register */
      uint8_t                           :  2; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PMUA_IRQ_FIRED_MASKED; /* +0x10C */

  union {
    struct {
      uint8_t  RETAIN                   :  1; /*!< Interrupt Enable */
      uint8_t  HIBERNATE                :  1; /*!< Interrupt Enable */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PWRMODE_IRQ_ENA; /* +0x110 */

  union {
    struct {
      uint8_t  RETAIN                   :  1; /*!< Flag Clear Register */
      uint8_t  HIBERNATE                :  1; /*!< Flag Clear Register */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PWRMODE_IRQ_CLR; /* +0x114 */

  union {
    struct {
      uint8_t  RETAIN                   :  1; /*!< Interrupt Fired Flag Register */
      uint8_t  HIBERNATE                :  1; /*!< Interrupt Fired Flag Register */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PWRMODE_IRQ_FIRED; /* +0x118 */

  union {
    struct {
      uint8_t  RETAIN                   :  1; /*!< Interrupt Fired Masked Flag Register */
      uint8_t  HIBERNATE                :  1; /*!< Interrupt Fired Masked Flag Register */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } PWRMODE_IRQ_FIRED_MASKED; /* +0x11C */

} PMUA_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of PMUA SFRS.
 */
#define PMUA_SFRS ((__IO PMUA_SFRS_t *)0x5001e000)

#endif /* end of __PMUA_SFR_H__ section */


