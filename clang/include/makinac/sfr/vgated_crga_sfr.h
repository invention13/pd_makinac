/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file vgated_crga_sfr.h
 */

#ifndef __VGATED_CRGA_SFR_H__
#define __VGATED_CRGA_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for VGATED_CRGA.
 */
typedef struct {

  uint8_t  SYS_CLK;                           /*<! System Clock Status +0x000 */
  uint8_t  _RESERVED_01[3];                   /* +0x001 */

  union {
    struct {
      uint8_t  SYS_CLK_SEL              :  8; /*!< System Clock RC Oscillator Select */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  DIV_SEL                  :  8; /*!< System Clock Divider Select */
      uint8_t  PRESCALE_ENA             :  1; /*!< System Prescaler Enable */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  RO_ENA                   :  1; /*!< Ring Oscillator Enable */
      uint8_t                           :  3; /*   (reserved) */
    };
    uint32_t WORD;
  } SYS_CLK_CTRL; /* +0x004 */

  uint8_t  SYSRESETREQ_ENA;                   /*<! System Reset Request Enable +0x008 */
  uint8_t  _RESERVED_09[3];                   /* +0x009 */

  union {
    struct {
      uint8_t  HARD                     :  8; /*!< Hard reset request */
      uint8_t  SOFT_RST                 :  8; /*!< Soft reset request */
      uint8_t  CAN                      :  8; /*!< CAN reset request */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } RESET_REQ; /* +0x00C */

  union {
    struct {
      uint8_t  POR                      :  1; /*!< Power On Reset flag */
      uint8_t  SOFT_RST                 :  1; /*!< Soft Reset Request flag */
      uint8_t  HARD                     :  1; /*!< Hard Reset Request flag */
      uint8_t  CLK_FAIL_PLL             :  1; /*!< PLL Clock Failure flag */
      uint8_t  CLK_FAIL_XO              :  1; /*!< XTAL Clock Failure flag */
      uint8_t  WDT_BARK                 :  1; /*!< Watch Dog Timer bark flag */
      uint8_t  WDT_WF                   :  1; /*!< Watch Dog Timer Windowing Failure flag */
      uint8_t  SYSRESETREQ              :  1; /*!< M4 System Reset Request Flag */
      uint8_t  PMU                      :  1; /*!< 5V PMU Digital Reset Flag */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  HIBERNATE                :  1; /*!< PMUA Hibernate Flag */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  CAN                      :  8; /*!< CAN Reset Request Flag */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CM4_RR; /* +0x010 */

  union {
    struct {
      uint8_t  POR                      :  1; /*!< Power On Reset flag clear */
      uint8_t  SOFT_RST                 :  1; /*!< Soft Reset Request flag clear */
      uint8_t  HARD                     :  1; /*!< Hard Reset Request flag clear */
      uint8_t  CLK_FAIL_PLL             :  1; /*!< PLL Clock Failure flag clear */
      uint8_t  CLK_FAIL_XO              :  1; /*!< XTAL Clock Failure flag clear */
      uint8_t  WDT_BARK                 :  1; /*!< Watch Dog Timer bark flag clear */
      uint8_t  WDT_WF                   :  1; /*!< Watch Dog Timer Windowing Failure flag clear */
      uint8_t  SYSRESETREQ              :  1; /*!< CM4 System Reset Request Flag Clear */
      uint8_t  PMU                      :  1; /*!< 5V PMU Digital Reset Flag Clear */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  HIBERNATE                :  1; /*!< HIBERNATE Flag Clear */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  CAN                      :  8; /*!< CAN Reset Request Flag Clear */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CM4_RR_CLR; /* +0x014 */

  union {
    struct {
      uint8_t  SEL                      :  8; /*!< CAN Clock Select */
      uint8_t  STATUS                   :  8; /*!< CAN Clock Status */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } CAN_CLK_CTRL; /* +0x018 */

  uint8_t  WDT_STRB_ENA;                      /*<! WDT Strobe Generator Enable +0x01C */
  uint8_t  _RESERVED_1D[3];                   /* +0x01D */

  union {
    struct {
      uint8_t  ENA                      :  8; /*!< PLL Clock Fail Enable */
      uint8_t  RST_ENA                  :  8; /*!< PLL Clock Fail Reset Enable Register */
      uint8_t  DIV_SEL                  :  3; /*!< Divider Select for the PLL clock failure detetction mechanism */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  CLK_SWITCH_ENA           :  1; /*!< Switch clock to RC on clock failure */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  COUNT                    :  8; /*!< PLL Clock Fail Count */
    };
    uint32_t WORD;
  } CLK_FAIL_PLL_CTRL; /* +0x020 */

  union {
    struct {
      uint8_t  ENA                      :  8; /*!< Xtal Clock Fail Enable */
      uint8_t  RST_ENA                  :  8; /*!< XTAL Clock Fail Reset Enable Register */
      uint8_t  DIV_SEL                  :  2; /*!< Divider Select for the XTAL clock failure detetction mechanism */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  CLK_SWITCH_ENA           :  1; /*!< Switch clock to RC on clock failure */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  COUNT                    :  8; /*!< XTAL Clock Fail Count */
    };
    uint32_t WORD;
  } CLK_FAIL_XO_CTRL; /* +0x024 */

  union {
    struct {
      uint8_t  CLK_FAIL_PLL             :  1; /*!< Interrupt Enable for the clock failure monitor on PLL clock */
      uint8_t  CLK_FAIL_XO              :  1; /*!< Interrupt Enable for the clock failure monitor on High Freq Xtal clock */
      uint8_t  PLL_LOCK                 :  1; /*!< Interrupt Enable for the PLL Lock signal */
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } IRQ_ENA; /* +0x028 */

  union {
    struct {
      uint8_t  CLK_FAIL_PLL             :  1; /*!< Flag Clear Register for the clock failure monitor on PLL clock */
      uint8_t  CLK_FAIL_XO              :  1; /*!< Flag Clear Register for the clock failure monitor on Xtal clock */
      uint8_t  PLL_LOCK                 :  1; /*!< Flag Clear Register for the PLL Lock */
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } IRQ_CLR; /* +0x02C */

  union {
    struct {
      uint8_t  CLK_FAIL_PLL             :  1; /*!< Interrupt Fired Flag Register for the clock failure monitor on PLL clock */
      uint8_t  CLK_FAIL_XO              :  1; /*!< Interrupt Fired Flag Register for the clock failure monitor on Xtal clock */
      uint8_t  PLL_LOCK                 :  1; /*!< Interrupt Fired Flag Register for the PLL Lock */
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } IRQ_FIRED; /* +0x030 */

  union {
    struct {
      uint8_t  CLK_FAIL_PLL             :  1; /*!< Interrupt Fired Masked Flag Register for the clock failure monitor on PLL clock */
      uint8_t  CLK_FAIL_XO              :  1; /*!< Interrupt Fired Masked Flag Register for the clock failure monitor on Xtal clock */
      uint8_t  PLL_LOCK                 :  1; /*!< Interrupt Fired Masked Flag Register for the PLL Lock */
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } IRQ_FIRED_MASKED; /* +0x034 */

} VGATED_CRGA_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of VGATED_CRGA SFRS.
 */
#define VGATED_CRGA_SFRS ((__IO VGATED_CRGA_SFRS_t *)0x50000400)

#endif /* end of __VGATED_CRGA_SFR_H__ section */


