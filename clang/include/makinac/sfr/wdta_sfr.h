/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file wdta_sfr.h
 */

#ifndef __WDTA_SFR_H__
#define __WDTA_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for WDTA.
 */
typedef struct {

  union {
    struct {
      uint8_t  RUNSTS                   :  1;
      uint8_t  BRKFLG                   :  1;
      uint8_t  WNDFLG                   :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  WNDMODE                  :  1;
      uint8_t  WARNIRQ                  :  1;
      uint8_t  SWFLGCLR                 :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  PRESCALER                :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CSR; /* +0x000 */

  uint16_t CLRPER;                            /* +0x004 */
  uint8_t  _RESERVED_06[2];                   /* +0x006 */

  uint32_t COMMAND;                           /* +0x008 */

  uint32_t CLEAR;                             /* +0x00C */

  uint16_t WINPER;                            /* +0x010 */
  uint8_t  _RESERVED_12[2];                   /* +0x012 */

} WDTA_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of WDTA SFRS.
 */
#define WDTA_SFRS ((__IO WDTA_SFRS_t *)0x50002000)

#endif /* end of __WDTA_SFR_H__ section */


