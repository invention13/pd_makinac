/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file sysctrla_sfr.h
 */

#ifndef __SYSCTRLA_SFR_H__
#define __SYSCTRLA_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for SYSCTRLA.
 */
typedef struct {

  uint32_t NAME;                              /*<! ASIC name +0x000 */

  uint16_t REV;                               /*<! Silicon Revision +0x004 */
  uint8_t  _RESERVED_06[2];                   /* +0x006 */

  uint32_t DEBUG_ACCESS_KEY;                  /* +0x008 */

  uint8_t  DEBUG_ACCESS_STATUS;               /* +0x00C */
  uint8_t  _RESERVED_0D[3];                   /* +0x00D */

  uint32_t TRIM_ACCESS_KEY;                   /* +0x010 */

  uint8_t  TRIM_ACCESS_STATUS;                /* +0x014 */
  uint8_t  _RESERVED_15[3];                   /* +0x015 */

  uint8_t  TRIM_MEM_WAIT_STATE_NUM;           /* +0x018 */
  uint8_t  _RESERVED_19[3];                   /* +0x019 */

  uint32_t _RESERVED_1C[57];

  uint32_t RETAIN0;                           /*<! Firmware scratch register 0 +0x100 */

  uint32_t RETAIN1;                           /*<! Firmware scratch register 1 +0x104 */

  uint32_t RETAIN2;                           /*<! Firmware scratch register 2 +0x108 */

  uint32_t RETAIN3;                           /*<! Firmware scratch register 3 +0x10C */

  uint32_t RETAIN4;                           /*<! Firmware scratch register 4 +0x110 */

  uint32_t RETAIN5;                           /*<! Firmware scratch register 5 +0x114 */

  uint32_t RETAIN6;                           /*<! Firmware scratch register 6 +0x118 */

  uint32_t RETAIN7;                           /*<! Firmware scratch register 7 +0x11C */

  uint32_t RETAIN8;                           /*<! Firmware scratch register 8 +0x120 */

  uint32_t RETAIN9;                           /*<! Firmware scratch register 9 +0x124 */

  uint32_t RETAINA;                           /*<! Firmware scratch register 10 (0xA) +0x128 */

  uint32_t RETAINB;                           /*<! Firmware scratch register 11 (0xB) +0x12C */

  uint32_t RETAINC;                           /*<! Firmware scratch register 12 (0xC) +0x130 */

  uint32_t RETAIND;                           /*<! Firmware scratch register 13 (0xD) +0x134 */

  uint32_t RETAINE;                           /*<! Firmware scratch register 14 (0xE) +0x138 */

  uint32_t RETAINF;                           /*<! Firmware scratch register 15 (0xF) +0x13C */

} SYSCTRLA_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of SYSCTRLA SFRS.
 */
#define SYSCTRLA_SFRS ((__IO SYSCTRLA_SFRS_t *)0x50000000)

#endif /* end of __SYSCTRLA_SFR_H__ section */


