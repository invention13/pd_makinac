/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file adc_ctrl_argon_sfr.h
 */

#ifndef __ADC_CTRL_ARGON_SFR_H__
#define __ADC_CTRL_ARGON_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for ADC_CTRL_ARGON.
 */
typedef struct {

  union {
    struct {
      uint8_t  NODE0IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE0IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE0DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE0SEMODE              :  1; /*!< semode */
      uint8_t  NODE0AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE0ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE0ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE0CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE0DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE0DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE0CTRLA; /* +0x000 */

  union {
    struct {
      uint16_t NODE0THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE0THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE0ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE0CTRLB; /* +0x004 */

  union {
    struct {
      uint16_t NODE0ADCWORD             : 16; /*!< Node 0 Results */
      uint8_t  NODE0THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE0THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE0CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE0IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE0DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE0DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE0STATUS; /* +0x008 */

  union {
    struct {
      uint8_t  NODE1IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE1IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE1DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE1SEMODE              :  1; /*!< semode */
      uint8_t  NODE1AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE1ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE1ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE1CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE1DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE1DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE1CTRLA; /* +0x00C */

  union {
    struct {
      uint16_t NODE1THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE1THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE1ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE1CTRLB; /* +0x010 */

  union {
    struct {
      uint16_t NODE1ADCWORD             : 16; /*!< Node 1 Results */
      uint8_t  NODE1THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE1THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE1CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE1IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE1DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE1DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE1STATUS; /* +0x014 */

  union {
    struct {
      uint8_t  NODE2IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE2IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE2DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE2SEMODE              :  1; /*!< semode */
      uint8_t  NODE2AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE2ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE2ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE2CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE2DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE2DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE2CTRLA; /* +0x018 */

  union {
    struct {
      uint16_t NODE2THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE2THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE2ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE2CTRLB; /* +0x01C */

  union {
    struct {
      uint16_t NODE2ADCWORD             : 16; /*!< Node 2 Results */
      uint8_t  NODE2THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE2THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE2CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE2IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE2DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE2DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE2STATUS; /* +0x020 */

  union {
    struct {
      uint8_t  NODE3IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE3IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE3DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE3SEMODE              :  1; /*!< semode */
      uint8_t  NODE3AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE3ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE3ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE3CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE3DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE3DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE3CTRLA; /* +0x024 */

  union {
    struct {
      uint16_t NODE3THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE3THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE3ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE3CTRLB; /* +0x028 */

  union {
    struct {
      uint16_t NODE3ADCWORD             : 16; /*!< Node 3 Results */
      uint8_t  NODE3THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE3THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE3CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE3IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE3DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE3DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE3STATUS; /* +0x02C */

  union {
    struct {
      uint8_t  NODE4IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE4IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE4DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE4SEMODE              :  1; /*!< semode */
      uint8_t  NODE4AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE4ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE4ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE4CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE4DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE4DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE4CTRLA; /* +0x030 */

  union {
    struct {
      uint16_t NODE4THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE4THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE4ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE4CTRLB; /* +0x034 */

  union {
    struct {
      uint16_t NODE4ADCWORD             : 16; /*!< Node 4 Results */
      uint8_t  NODE4THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE4THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE4CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE4IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE4DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE4DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE4STATUS; /* +0x038 */

  union {
    struct {
      uint8_t  NODE5IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE5IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE5DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE5SEMODE              :  1; /*!< semode */
      uint8_t  NODE5AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE5ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE5ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE5CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE5DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE5DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE5CTRLA; /* +0x03C */

  union {
    struct {
      uint16_t NODE5THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE5THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE5ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE5CTRLB; /* +0x040 */

  union {
    struct {
      uint16_t NODE5ADCWORD             : 16; /*!< Node 5 Results */
      uint8_t  NODE5THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE5THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE5CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE5IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE5DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE5DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE5STATUS; /* +0x044 */

  union {
    struct {
      uint8_t  NODE6IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE6IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE6DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE6SEMODE              :  1; /*!< semode */
      uint8_t  NODE6AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE6ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE6ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE6CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE6DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE6DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE6CTRLA; /* +0x048 */

  union {
    struct {
      uint16_t NODE6THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE6THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE6ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE6CTRLB; /* +0x04C */

  union {
    struct {
      uint16_t NODE6ADCWORD             : 16; /*!< Node 6 Results */
      uint8_t  NODE6THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE6THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE6CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE6IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE6DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE6DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE6STATUS; /* +0x050 */

  union {
    struct {
      uint8_t  NODE7IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE7IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE7DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE7SEMODE              :  1; /*!< semode */
      uint8_t  NODE7AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE7ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE7ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE7CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE7DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE7DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE7CTRLA; /* +0x054 */

  union {
    struct {
      uint16_t NODE7THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE7THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE7ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE7CTRLB; /* +0x058 */

  union {
    struct {
      uint16_t NODE7ADCWORD             : 16; /*!< Node 7 Results */
      uint8_t  NODE7THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE7THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE7CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE7IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE7DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE7DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE7STATUS; /* +0x05C */

  union {
    struct {
      uint8_t  NODE8IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE8IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE8DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE8SEMODE              :  1; /*!< semode */
      uint8_t  NODE8AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE8ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE8ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE8CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE8DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE8DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE8CTRLA; /* +0x060 */

  union {
    struct {
      uint16_t NODE8THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE8THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE8ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE8CTRLB; /* +0x064 */

  union {
    struct {
      uint16_t NODE8ADCWORD             : 16; /*!< Node 8 Results */
      uint8_t  NODE8THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE8THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE8CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE8IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE8DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE8DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE8STATUS; /* +0x068 */

  union {
    struct {
      uint8_t  NODE9IRQORARMED          :  1; /*!< Irq_or Armed */
      uint8_t  NODE9IRQIRARMED          :  1; /*!< Irq_ir Armed */
      uint8_t  NODE9DBCIRQ              :  4; /*!< Dbcirq */
      uint8_t  NODE9SEMODE              :  1; /*!< semode */
      uint8_t  NODE9AUTOEN              :  1; /*!< Bias enable mode */
      uint8_t  NODE9ADCMUX              :  6; /*!< Adcmux */
      uint8_t  NODE9ATTEN               :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE9CNVDONEARMED        :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE9DLYA2N              : 12; /*!< Dlya2n */
      uint8_t  NODE9DLYM2A              :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE9CTRLA; /* +0x06C */

  union {
    struct {
      uint16_t NODE9THRLOW              : 16; /*!< Thr Low */
      uint16_t NODE9THRHIGH             : 12; /*!< Thr High */
      uint8_t  NODE9ADCSMPCYC           :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE9CTRLB; /* +0x070 */

  union {
    struct {
      uint16_t NODE9ADCWORD             : 16; /*!< Node 9 Results */
      uint8_t  NODE9THRLOWEXCD          :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE9THRHIGHEXCD         :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE9CONVDONE            :  1; /*!< Conversion Done */
      uint8_t  NODE9IREXCD              :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE9DBCCNTRIR           :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE9DBCCNTROR           :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE9STATUS; /* +0x074 */

  union {
    struct {
      uint8_t  NODE10IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE10IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE10DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE10SEMODE             :  1; /*!< semode */
      uint8_t  NODE10AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE10ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE10ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE10CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE10DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE10DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE10CTRLA; /* +0x078 */

  union {
    struct {
      uint16_t NODE10THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE10THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE10ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE10CTRLB; /* +0x07C */

  union {
    struct {
      uint16_t NODE10ADCWORD            : 16; /*!< Node 10 Results */
      uint8_t  NODE10THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE10THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE10CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE10IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE10DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE10DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE10STATUS; /* +0x080 */

  union {
    struct {
      uint8_t  NODE11IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE11IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE11DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE11SEMODE             :  1; /*!< semode */
      uint8_t  NODE11AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE11ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE11ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE11CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE11DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE11DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE11CTRLA; /* +0x084 */

  union {
    struct {
      uint16_t NODE11THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE11THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE11ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE11CTRLB; /* +0x088 */

  union {
    struct {
      uint16_t NODE11ADCWORD            : 16; /*!< Node 11 Results */
      uint8_t  NODE11THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE11THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE11CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE11IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE11DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE11DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE11STATUS; /* +0x08C */

  union {
    struct {
      uint8_t  NODE12IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE12IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE12DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE12SEMODE             :  1; /*!< semode */
      uint8_t  NODE12AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE12ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE12ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE12CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE12DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE12DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE12CTRLA; /* +0x090 */

  union {
    struct {
      uint16_t NODE12THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE12THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE12ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE12CTRLB; /* +0x094 */

  union {
    struct {
      uint16_t NODE12ADCWORD            : 16; /*!< Node 12 Results */
      uint8_t  NODE12THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE12THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE12CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE12IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE12DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE12DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE12STATUS; /* +0x098 */

  union {
    struct {
      uint8_t  NODE13IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE13IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE13DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE13SEMODE             :  1; /*!< semode */
      uint8_t  NODE13AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE13ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE13ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE13CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE13DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE13DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE13CTRLA; /* +0x09C */

  union {
    struct {
      uint16_t NODE13THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE13THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE13ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE13CTRLB; /* +0x0A0 */

  union {
    struct {
      uint16_t NODE13ADCWORD            : 16; /*!< Node 13 Results */
      uint8_t  NODE13THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE13THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE13CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE13IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE13DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE13DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE13STATUS; /* +0x0A4 */

  union {
    struct {
      uint8_t  NODE14IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE14IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE14DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE14SEMODE             :  1; /*!< semode */
      uint8_t  NODE14AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE14ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE14ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE14CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE14DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE14DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE14CTRLA; /* +0x0A8 */

  union {
    struct {
      uint16_t NODE14THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE14THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE14ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE14CTRLB; /* +0x0AC */

  union {
    struct {
      uint16_t NODE14ADCWORD            : 16; /*!< Node 14 Results */
      uint8_t  NODE14THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE14THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE14CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE14IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE14DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE14DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE14STATUS; /* +0x0B0 */

  union {
    struct {
      uint8_t  NODE15IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE15IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE15DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE15SEMODE             :  1; /*!< semode */
      uint8_t  NODE15AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE15ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE15ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE15CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE15DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE15DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE15CTRLA; /* +0x0B4 */

  union {
    struct {
      uint16_t NODE15THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE15THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE15ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE15CTRLB; /* +0x0B8 */

  union {
    struct {
      uint16_t NODE15ADCWORD            : 16; /*!< Node 15 Results */
      uint8_t  NODE15THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE15THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE15CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE15IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE15DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE15DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE15STATUS; /* +0x0BC */

  union {
    struct {
      uint8_t  NODE16IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE16IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE16DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE16SEMODE             :  1; /*!< semode */
      uint8_t  NODE16AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE16ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE16ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE16CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE16DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE16DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE16CTRLA; /* +0x0C0 */

  union {
    struct {
      uint16_t NODE16THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE16THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE16ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE16CTRLB; /* +0x0C4 */

  union {
    struct {
      uint16_t NODE16ADCWORD            : 16; /*!< Node 16 Results */
      uint8_t  NODE16THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE16THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE16CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE16IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE16DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE16DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE16STATUS; /* +0x0C8 */

  union {
    struct {
      uint8_t  NODE17IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE17IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE17DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE17SEMODE             :  1; /*!< semode */
      uint8_t  NODE17AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE17ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE17ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE17CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE17DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE17DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE17CTRLA; /* +0x0CC */

  union {
    struct {
      uint16_t NODE17THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE17THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE17ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE17CTRLB; /* +0x0D0 */

  union {
    struct {
      uint16_t NODE17ADCWORD            : 16; /*!< Node 17 Results */
      uint8_t  NODE17THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE17THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE17CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE17IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE17DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE17DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE17STATUS; /* +0x0D4 */

  union {
    struct {
      uint8_t  NODE18IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE18IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE18DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE18SEMODE             :  1; /*!< semode */
      uint8_t  NODE18AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE18ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE18ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE18CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE18DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE18DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE18CTRLA; /* +0x0D8 */

  union {
    struct {
      uint16_t NODE18THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE18THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE18ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE18CTRLB; /* +0x0DC */

  union {
    struct {
      uint16_t NODE18ADCWORD            : 16; /*!< Node 18 Results */
      uint8_t  NODE18THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE18THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE18CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE18IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE18DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE18DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE18STATUS; /* +0x0E0 */

  union {
    struct {
      uint8_t  NODE19IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE19IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE19DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE19SEMODE             :  1; /*!< semode */
      uint8_t  NODE19AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE19ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE19ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE19CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE19DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE19DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE19CTRLA; /* +0x0E4 */

  union {
    struct {
      uint16_t NODE19THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE19THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE19ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE19CTRLB; /* +0x0E8 */

  union {
    struct {
      uint16_t NODE19ADCWORD            : 16; /*!< Node 19 Results */
      uint8_t  NODE19THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE19THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE19CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE19IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE19DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE19DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE19STATUS; /* +0x0EC */

  union {
    struct {
      uint8_t  NODE20IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE20IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE20DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE20SEMODE             :  1; /*!< semode */
      uint8_t  NODE20AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE20ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE20ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE20CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE20DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE20DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE20CTRLA; /* +0x0F0 */

  union {
    struct {
      uint16_t NODE20THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE20THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE20ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE20CTRLB; /* +0x0F4 */

  union {
    struct {
      uint16_t NODE20ADCWORD            : 16; /*!< Node 20 Results */
      uint8_t  NODE20THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE20THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE20CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE20IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE20DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE20DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE20STATUS; /* +0x0F8 */

  union {
    struct {
      uint8_t  NODE21IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE21IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE21DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE21SEMODE             :  1; /*!< semode */
      uint8_t  NODE21AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE21ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE21ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE21CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE21DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE21DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE21CTRLA; /* +0x0FC */

  union {
    struct {
      uint16_t NODE21THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE21THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE21ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE21CTRLB; /* +0x100 */

  union {
    struct {
      uint16_t NODE21ADCWORD            : 16; /*!< Node 21 Results */
      uint8_t  NODE21THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE21THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE21CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE21IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE21DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE21DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE21STATUS; /* +0x104 */

  union {
    struct {
      uint8_t  NODE22IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE22IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE22DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE22SEMODE             :  1; /*!< semode */
      uint8_t  NODE22AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE22ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE22ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE22CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE22DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE22DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE22CTRLA; /* +0x108 */

  union {
    struct {
      uint16_t NODE22THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE22THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE22ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE22CTRLB; /* +0x10C */

  union {
    struct {
      uint16_t NODE22ADCWORD            : 16; /*!< Node 22 Results */
      uint8_t  NODE22THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE22THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE22CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE22IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE22DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE22DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE22STATUS; /* +0x110 */

  union {
    struct {
      uint8_t  NODE23IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE23IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE23DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE23SEMODE             :  1; /*!< semode */
      uint8_t  NODE23AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE23ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE23ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE23CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE23DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE23DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE23CTRLA; /* +0x114 */

  union {
    struct {
      uint16_t NODE23THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE23THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE23ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE23CTRLB; /* +0x118 */

  union {
    struct {
      uint16_t NODE23ADCWORD            : 16; /*!< Node 23 Results */
      uint8_t  NODE23THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE23THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE23CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE23IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE23DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE23DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE23STATUS; /* +0x11C */

  union {
    struct {
      uint8_t  NODE24IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE24IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE24DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE24SEMODE             :  1; /*!< semode */
      uint8_t  NODE24AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE24ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE24ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE24CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE24DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE24DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE24CTRLA; /* +0x120 */

  union {
    struct {
      uint16_t NODE24THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE24THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE24ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE24CTRLB; /* +0x124 */

  union {
    struct {
      uint16_t NODE24ADCWORD            : 16; /*!< Node 24 Results */
      uint8_t  NODE24THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE24THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE24CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE24IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE24DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE24DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE24STATUS; /* +0x128 */

  union {
    struct {
      uint8_t  NODE25IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE25IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE25DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE25SEMODE             :  1; /*!< semode */
      uint8_t  NODE25AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE25ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE25ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE25CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE25DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE25DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE25CTRLA; /* +0x12C */

  union {
    struct {
      uint16_t NODE25THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE25THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE25ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE25CTRLB; /* +0x130 */

  union {
    struct {
      uint16_t NODE25ADCWORD            : 16; /*!< Node 25 Results */
      uint8_t  NODE25THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE25THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE25CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE25IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE25DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE25DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE25STATUS; /* +0x134 */

  union {
    struct {
      uint8_t  NODE26IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE26IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE26DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE26SEMODE             :  1; /*!< semode */
      uint8_t  NODE26AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE26ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE26ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE26CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE26DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE26DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE26CTRLA; /* +0x138 */

  union {
    struct {
      uint16_t NODE26THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE26THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE26ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE26CTRLB; /* +0x13C */

  union {
    struct {
      uint16_t NODE26ADCWORD            : 16; /*!< Node 26 Results */
      uint8_t  NODE26THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE26THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE26CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE26IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE26DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE26DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE26STATUS; /* +0x140 */

  union {
    struct {
      uint8_t  NODE27IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE27IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE27DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE27SEMODE             :  1; /*!< semode */
      uint8_t  NODE27AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE27ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE27ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE27CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE27DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE27DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE27CTRLA; /* +0x144 */

  union {
    struct {
      uint16_t NODE27THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE27THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE27ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE27CTRLB; /* +0x148 */

  union {
    struct {
      uint16_t NODE27ADCWORD            : 16; /*!< Node 27 Results */
      uint8_t  NODE27THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE27THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE27CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE27IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE27DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE27DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE27STATUS; /* +0x14C */

  union {
    struct {
      uint8_t  NODE28IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE28IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE28DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE28SEMODE             :  1; /*!< semode */
      uint8_t  NODE28AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE28ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE28ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE28CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE28DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE28DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE28CTRLA; /* +0x150 */

  union {
    struct {
      uint16_t NODE28THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE28THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE28ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE28CTRLB; /* +0x154 */

  union {
    struct {
      uint16_t NODE28ADCWORD            : 16; /*!< Node 28 Results */
      uint8_t  NODE28THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE28THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE28CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE28IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE28DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE28DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE28STATUS; /* +0x158 */

  union {
    struct {
      uint8_t  NODE29IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE29IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE29DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE29SEMODE             :  1; /*!< semode */
      uint8_t  NODE29AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE29ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE29ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE29CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE29DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE29DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE29CTRLA; /* +0x15C */

  union {
    struct {
      uint16_t NODE29THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE29THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE29ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE29CTRLB; /* +0x160 */

  union {
    struct {
      uint16_t NODE29ADCWORD            : 16; /*!< Node 29 Results */
      uint8_t  NODE29THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE29THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE29CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE29IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE29DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE29DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE29STATUS; /* +0x164 */

  union {
    struct {
      uint8_t  NODE30IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE30IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE30DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE30SEMODE             :  1; /*!< semode */
      uint8_t  NODE30AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE30ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE30ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE30CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE30DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE30DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE30CTRLA; /* +0x168 */

  union {
    struct {
      uint16_t NODE30THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE30THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE30ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE30CTRLB; /* +0x16C */

  union {
    struct {
      uint16_t NODE30ADCWORD            : 16; /*!< Node 30 Results */
      uint8_t  NODE30THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE30THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE30CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE30IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE30DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE30DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE30STATUS; /* +0x170 */

  union {
    struct {
      uint8_t  NODE31IRQORARMED         :  1; /*!< Irq_or Armed */
      uint8_t  NODE31IRQIRARMED         :  1; /*!< Irq_ir Armed */
      uint8_t  NODE31DBCIRQ             :  4; /*!< Dbcirq */
      uint8_t  NODE31SEMODE             :  1; /*!< semode */
      uint8_t  NODE31AUTOEN             :  1; /*!< Bias enable mode */
      uint8_t  NODE31ADCMUX             :  6; /*!< Adcmux */
      uint8_t  NODE31ATTEN              :  1; /*!< ADC input Attenuation setting */
      uint8_t  NODE31CNVDONEARMED       :  1; /*!< ADC Conversion Done Armed */
      uint16_t NODE31DLYA2N             : 12; /*!< Dlya2n */
      uint8_t  NODE31DLYM2A             :  4; /*!< Dlym2a */
    };
    uint32_t WORD;
  } NODE31CTRLA; /* +0x174 */

  union {
    struct {
      uint16_t NODE31THRLOW             : 16; /*!< Thr Low */
      uint16_t NODE31THRHIGH            : 12; /*!< Thr High */
      uint8_t  NODE31ADCSMPCYC          :  3; /*!< Adcsmp Cyc */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } NODE31CTRLB; /* +0x178 */

  union {
    struct {
      uint16_t NODE31ADCWORD            : 16; /*!< Node 31 Results */
      uint8_t  NODE31THRLOWEXCD         :  1; /*!< Threshold Low Exceeded */
      uint8_t  NODE31THRHIGHEXCD        :  1; /*!< Threshold High Exceeded */
      uint8_t  NODE31CONVDONE           :  1; /*!< Conversion Done */
      uint8_t  NODE31IREXCD             :  1; /*!< ADC Value In Range */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  NODE31DBCCNTRIR          :  4; /*!< In Range Debounce Counter */
      uint8_t  NODE31DBCCNTROR          :  4; /*!< Out of Range Debounce Counter */
    };
    uint32_t WORD;
  } NODE31STATUS; /* +0x17C */

  uint32_t ADCIRQFLAG;                        /*<! IRQ Flag Status +0x180 */

  uint32_t CLRADCIRQFLAG;                     /*<! Clear the Node IRQ flag +0x184 */

  uint32_t NODEACTIVE;                        /*<! Node Active +0x188 */

  uint32_t NODEONESHOT;                       /*<! Node Oneshot +0x18C */

  union {
    struct {
      uint8_t  ADCENABLE                :  1; /*!< ADC Enable */
      uint8_t  ADCENAPTAT               :  1; /*!< ADC Enable PTAT */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  ADCDATACLR               :  1; /*!< ADC Data Clear */
      uint8_t                           :  4; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ADCCTRL; /* +0x190 */

  union {
    struct {
      uint8_t  ONESHOTDONEARMED         :  1; /*!< ADC Oneshot done interrupt is aremed */
      uint8_t  ACTIVEDONEARMED          :  1; /*!< ADC Active done interrupt is aremed */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ADCIRQEN; /* +0x194 */

  union {
    struct {
      uint8_t  CLRONESHOTDONE           :  1; /*!< ADC Data Clear */
      uint8_t  CLRACTIVEDONE            :  1; /*!< ADC Data Clear */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ADCIRQCLR; /* +0x198 */

  union {
    struct {
      uint8_t  ONESHOTDONE              :  1; /*!< Oneshot Done */
      uint8_t  ACTIVEDONE               :  1; /*!< Active Done */
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } ADCIRQSTATUS; /* +0x19C */

} ADC_CTRL_ARGON_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of ADC_CTRL_ARGON SFRS.
 */
#define ADC_CTRL_ARGON_SFRS ((__IO ADC_CTRL_ARGON_SFRS_t *)0x50003000)

#endif /* end of __ADC_CTRL_ARGON_SFR_H__ section */


