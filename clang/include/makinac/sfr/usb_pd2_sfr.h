/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file usb_pd2_sfr.h
 */

#ifndef __USB_PD2_SFR_H__
#define __USB_PD2_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for USB_PD2.
 */
typedef struct {

  union {
    struct {
      uint8_t  MINOR_REV_ID             :  3; /*!< minor revision ID */
      uint8_t  MAJOR_REV_ID             :  3; /*!< major revision ID */
      uint8_t  VENDOR_ID                :  2; /*!< Vendor ID */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  USB_CTRL3_RESET_PHY      :  1; /*!< USB Control3 Reset PHY */
      uint8_t                           :  5; /*   (reserved) */
    };
    uint32_t WORD;
  } VER_CTRL; /* +0x000 */

  union {
    struct {
      uint8_t  CCPX_PU_ENA_CC1          :  1;
      uint8_t  CCPX_VCONN_ENA_CC1       :  1;
      uint8_t  CCPX_DSCH_ENA_CC1        :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  CCPX_PU_ENA_CC2          :  1;
      uint8_t  CCPX_VCONN_ENA_CC2       :  1;
      uint8_t  CCPX_DSCH_ENA_CC2        :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  CCPX_TX_MUX_SEL          :  1;
      uint8_t  VCONN_SWAP_ON            :  1; /*!< Vconn swap on */
      uint8_t  VCONN_SWAP_OFF           :  1; /*!< Vconn swap off */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  SET_FSM_UNATT_SRC        :  1;
      uint8_t  SET_FSM_DISABLE          :  1;
      uint8_t  SET_FSM_ERR_REC          :  1;
      uint8_t  SET_FSM_RESET            :  1;
      uint8_t                           :  4; /*   (reserved) */
    };
    uint32_t WORD;
  } CC_CTRL; /* +0x004 */

  union {
    struct {
      uint8_t  USB_TYPEC_STATE          :  8; /*!< usb_type_c_state */
      uint8_t  USB_TYPEC_ACTIVE         :  8; /*!< usb_type_c_active */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } USB_STATUS; /* +0x008 */

  union {
    struct {
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  USB_CC1_STATUS_SRC_RX    :  2; /*!< CC1 STATUS Source Rx */
      uint8_t                           :  1; /*   (reserved) */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  USB_CC2_STATUS_SRC_RX    :  2; /*!< CC2 STATUS Source Rx */
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } USB_CC_CMP_AND_STATUS; /* +0x00C */

  uint32_t _RESERVED_10;

  union {
    struct {
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  CCPX_PU_TRIM             :  4;
      uint8_t  CCPX_RX_TRIM             :  4;
      uint8_t  CCPX_TX_TRIM             :  2;
      uint8_t  CCPX_TX_VHI_TRIM         :  2;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  CCPX_VCONN_TRIM          :  8;
    };
    uint32_t WORD;
  } AFE_TRIM; /* +0x014 */

  union {
    struct {
      uint16_t CSC_32_KHZ_PERIOD        : 16;
      uint8_t  TX_BIT_PERIOD            :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CLOCK_CONFIG; /* +0x018 */

  union {
    struct {
      uint8_t  I_TX_DISCARD             :  1;
      uint8_t  I_PD_TX_FAIL             :  1;
      uint8_t  I_PD_TX_OK               :  1;
      uint8_t  I_PD_CR_RX               :  1;
      uint8_t  I_PD_HR_RX               :  1;
      uint8_t  I_PD_RX                  :  1;
      uint8_t  I_CC_CHANGE              :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } INTERRUPT_STATUS; /* +0x01C */

  union {
    struct {
      uint8_t  M_TX_DISCARD             :  1;
      uint8_t  M_PD_TX_FAIL             :  1;
      uint8_t  M_PD_TX_OK               :  1;
      uint8_t  M_PD_CR_RX               :  1;
      uint8_t  M_PD_HR_RX               :  1;
      uint8_t  M_PD_RX                  :  1;
      uint8_t  M_CC_CHANGE              :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } INTERRUPT_ENABLE; /* +0x020 */

  union {
    struct {
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  PD_CFG_VBUS_HIGH_VOLT    :  1;
      uint8_t  PD_CFG_ID_INSERT         :  1;
      uint8_t  PD_CFG_ENABLE_SOP_TYPE   :  8;
      uint8_t  PD_CFG_P_PWR_ROLE_SOP    :  1; /*!< pd_cfg_pow_role_sop */
      uint8_t  PD_CFG_P_DATA_ROLE_SOP   :  1; /*!< pd_cfg_data_role_sop */
      uint8_t  PD_CFG_P_PWR_ROLE_PR     :  1; /*!< pd_cfg_pwr_role_pr */
      uint8_t  PD_CFG_P_DATA_ROLE_PR    :  1; /*!< pd_cfg_data_role_pr */
      uint8_t  PD_CFG_P_PWR_ROLE_DP     :  1; /*!< pd_cfg_pwr_role_dp */
      uint8_t  PD_CFG_P_DATA_ROLE_DP    :  1; /*!< pd_cfg_data_role_dp */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } PD_CFG; /* +0x024 */

  union {
    struct {
      uint8_t  TX_READY                 :  8;
      uint8_t  TX_RESULT                :  8;
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } TX_STATUS_INFO; /* +0x028 */

  union {
    struct {
      uint8_t  RX_CLEAR                 :  8;
      uint8_t  RX_DATA_AVAILABLE        :  8; /*!< rx data_available */
      uint8_t  RX_RESULT                :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } RX_STATUS_INFO; /* +0x02C */

  union {
    struct {
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  TX_RETRIES               :  3;
      uint8_t                           :  6; /*   (reserved) */
      uint16_t INTER_FRAME_GAP          : 10;
    };
    uint32_t WORD;
  } TX_CONFIG; /* +0x030 */

  union {
    struct {
      uint8_t  PD_DEBOUNCE_VALUE        :  8;
      uint8_t  VCONN_DSCHRG_VALUE       :  8;
      uint8_t  CC_DEBOUNCE_VALUE        :  8;
      uint8_t  ERR_RECOVERY_VALUE       :  8;
    };
    uint32_t WORD;
  } CC_TIMER; /* +0x034 */

  union {
    struct {
      uint8_t  HOLD_OFF_VALUE           :  4;
      uint8_t  UNATT_SRC_INT_EN         :  1;
      uint8_t  ATTWAIT_SRC_INT_EN       :  1;
      uint8_t  ATT_SRC_INT_EN           :  1;
      uint8_t  UNATTWAIT_SRC_INT_EN     :  1;
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  CCPX_LP_DET_ENA          :  1;
      uint8_t  CCPX_LP_DET_RA_ENA       :  1;
      uint8_t  CCPX_LP_DET_VREF_SEL     :  2;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  CCPX_RX_ENA              :  1;
      uint8_t  CCPX_TX_ENA_INIT         :  1;
      uint8_t  CCPX_OP_MODE_ENA         :  1;
      uint8_t  CCPX_PU_SEL              :  2;
      uint8_t  CCPX_RX_FD_ENA           :  1;
      uint8_t  PU_CONTROL_EN            :  1;
      uint8_t  VCONN_CONTROL_EN         :  1;
      uint8_t  DSCHRG_CONTROL_EN        :  1;
      uint8_t  ORIENT_CONTROL_EN        :  1;
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } CC_CONFIG; /* +0x038 */

  uint8_t  BIST_CARR_MODE_DURATION;           /*<! bist_carrier_mode_duration +0x03C */
  uint8_t  _RESERVED_3D[3];                   /* +0x03D */

  uint16_t TX_HEADER;                         /* +0x040 */
  uint8_t  _RESERVED_42[2];                   /* +0x042 */

  union {
    struct {
      uint8_t  TX_SOP                   :  8;
      uint8_t  TX_SEND_SOP              :  1;
      uint8_t  TX_SEND_HARD_RESET       :  1;
      uint8_t  TX_SEND_CABLE_RESET      :  1;
      uint8_t  TX_SEND_BIST             :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } TX_SOP; /* +0x044 */

  uint16_t RECEIVED_HEADER;                   /* +0x048 */
  uint8_t  _RESERVED_4A[2];                   /* +0x04A */

  union {
    struct {
      uint8_t  RX_SOP                   :  3;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  SOP_RECEIVED             :  1;
      uint8_t                           :  1; /*   (reserved) */
      uint8_t  HARD_RESET_RECEIVED      :  1;
      uint8_t  CABLE_RESET_RECEIVED     :  1;
      uint8_t                           :  5; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } RX_SOP; /* +0x04C */

  union {
    struct {
      uint8_t  BFA_WINDOW_OPEN_VALUE    :  8;
      uint8_t  BFA_WINDOW_CLOSE_VALUE   :  8;
      uint8_t  BFA_MAX_HALF_BIT_VALUE   :  8;
      uint8_t  BFA_PRE_SYNC_VALUE       :  8;
    };
    uint32_t WORD;
  } BFA_CONFIG; /* +0x050 */

  union {
    struct {
      uint8_t  SAMPLE_SELECT            :  3;
      uint8_t  RX_CLK_DIVIDER           :  1;
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  RX_THRESHOLD             :  8;
      uint8_t                           :  8; /*   (reserved) */
      uint8_t                           :  6; /*   (reserved) */
      uint8_t  RX_MODE                  :  1;
      uint8_t                           :  1; /*   (reserved) */
    };
    uint32_t WORD;
  } BMC_EDGE_REC_CONFIG; /* +0x054 */

  uint16_t PHY_LINE_IDLE_VALUE;               /* +0x058 */
  uint8_t  _RESERVED_5A[2];                   /* +0x05A */

  uint32_t _RESERVED_5C[41];

  uint32_t TX_BUFFER_DATA_1;                  /*<! tx_data1 +0x100 */

  uint32_t TX_BUFFER_DATA_2;                  /*<! tx_data2 +0x104 */

  uint32_t TX_BUFFER_DATA_3;                  /*<! tx_data3 +0x108 */

  uint32_t TX_BUFFER_DATA_4;                  /*<! tx_data4 +0x10C */

  uint32_t TX_BUFFER_DATA_5;                  /*<! tx_data5 +0x110 */

  uint32_t TX_BUFFER_DATA_6;                  /*<! tx_data6 +0x114 */

  uint32_t TX_BUFFER_DATA_7;                  /*<! tx_data7 +0x118 */

  uint32_t TX_BUFFER_DATA_8;                  /*<! tx_data8 +0x11C */

  uint32_t _RESERVED_120[56];

  uint32_t RX_DATA_BUFFER_1;                  /*<! rx_data1 +0x200 */

  uint32_t RX_DATA_BUFFER_2;                  /*<! rx_data2 +0x204 */

  uint32_t RX_DATA_BUFFER_3;                  /*<! rx_data3 +0x208 */

  uint32_t RX_DATA_BUFFER_4;                  /*<! rx_data4 +0x20C */

  uint32_t RX_DATA_BUFFER_5;                  /*<! rx_data5 +0x210 */

  uint32_t RX_DATA_BUFFER_6;                  /*<! rx_data6 +0x214 */

  uint32_t RX_DATA_BUFFER_7;                  /*<! rx_data7 +0x218 */

  uint32_t RX_DATA_BUFFER_8;                  /*<! rx_data8 +0x21C */

} USB_PD2_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of USB_PD2 SFRS.
 */
#define USB_PD2_SFRS ((__IO USB_PD2_SFRS_t *)0x50009000)

#endif /* end of __USB_PD2_SFR_H__ section */


