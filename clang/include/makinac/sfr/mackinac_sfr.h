/* this header brings in the individual files that define the
 * struct typdefs for all the special function registers of MACKINAC
 */

#ifndef __MACKINAC_SFR_H__
#define __MACKINAC_SFR_H__

#include "sysctrla_sfr.h"
#include "vgated_crga_sfr.h"
#include "ioctrla_sfr.h"
#include "efuse_ctrl_gallium_sfr.h"
#include "gpio_sfr.h"
#include "wdta_sfr.h"
#include "pwm0_sfr.h"
#include "pwm1_sfr.h"
#include "evthold_sfr.h"
#include "adc_ctrl_argon_sfr.h"
#include "linm_sfr.h"
#include "lins_sfr.h"
#include "uart_sfr.h"
#include "i2c_sfr.h"
#include "usb_pd1_sfr.h"
#include "usb_pd2_sfr.h"
#include "pmua_sfr.h"

#endif
