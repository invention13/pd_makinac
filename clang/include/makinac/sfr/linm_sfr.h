/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file linm_sfr.h
 */

#ifndef __LINM_SFR_H__
#define __LINM_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for LINM.
 */
typedef struct {

  union {
    struct {
      uint8_t  DATABYTE0                :  8; /*!< Data Byte 0 */
      uint8_t  DATABYTE1                :  8; /*!< Data Byte 1 */
      uint8_t  DATABYTE2                :  8; /*!< Data Byte 2 */
      uint8_t  DATABYTE3                :  8; /*!< Data Byte 3 */
    };
    uint32_t WORD;
  } DATABYTE03; /* +0x000 */

  union {
    struct {
      uint8_t  DATABYTE4                :  8; /*!< Data Byte 4 */
      uint8_t  DATABYTE5                :  8; /*!< Data Byte 5 */
      uint8_t  DATABYTE6                :  8; /*!< Data Byte 6 */
      uint8_t  DATABYTE7                :  8; /*!< Data Byte 7 */
    };
    uint32_t WORD;
  } DATABYTE47; /* +0x004 */

  union {
    struct {
      union {
        struct {
          uint8_t  STARTREQ             :  1; /*!< START Request */
          uint8_t  WAKEUPREQ            :  1; /*!< WakeUp Request */
          uint8_t  RSTERR               :  1; /*!< Reset Error */
          uint8_t  RSTINT               :  1; /*!< Reset interrupt */
          uint8_t                       :  1; /*   (reserved) */
          uint8_t  TRANSMIT             :  1; /*!< Transmit Operation */
          uint8_t  SLEEP                :  1; /*!< Sleep Request */
          uint8_t                       :  1; /*   (reserved) */
        };
        uint8_t BYTE;
      } CTRL;
      union {
        struct {
          uint8_t  COMPLETE             :  1;
          uint8_t  WAKEUP               :  1;
          uint8_t  ERROR                :  1; /*!< Lin Error */
          uint8_t  INTR                 :  1; /*!< Interupt Request */
          uint8_t                       :  3; /*   (reserved) */
          uint8_t  ACTIVE               :  1; /*!< Lin Bus Active */
        };
        uint8_t BYTE;
      } STATUS;
      union {
        struct {
          uint8_t  BITMON               :  1; /*!< Bit Error */
          uint8_t  CHK                  :  1; /*!< Checksum Error */
          uint8_t  TIMEOUT              :  1; /*!< Timeout Error */
          uint8_t                       :  5; /*   (reserved) */
        };
        uint8_t BYTE;
      } ERROR;
      union {
        struct {
          uint8_t  LENGTH               :  4; /*!< Data Length */
          uint8_t                       :  3; /*   (reserved) */
          uint8_t  ENHCHK               :  1; /*!< Enhancement Check */
        };
        uint8_t BYTE;
      } DL;
    };
    uint32_t WORD;
  } CSR; /* +0x008 */

  union {
    struct {
      uint16_t BTDIV                    :  9; /*!< Bt Div */
      uint8_t  BTMULT                   :  5; /*!< Bt Mult */
      uint8_t  PRESCL                   :  2; /*!< Prescaler */
      uint8_t  ID                       :  8;
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CONFIG; /* +0x00C */

} LINM_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of LINM SFRS.
 */
#define LINM_SFRS ((__IO LINM_SFRS_t *)0x50004000)

#endif /* end of __LINM_SFR_H__ section */


