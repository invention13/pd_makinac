/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file pwm1_sfr.h
 */

#ifndef __PWM1_SFR_H__
#define __PWM1_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for PWM1.
 */
typedef struct {

  uint8_t  _RESERVED_00;                      /* +0x000 */
  uint8_t  PRESCALESEL;                       /*<! Prescaler select +0x001 */
  uint8_t  _RESERVED_02[2];                   /* +0x002 */

  union {
    struct {
      uint8_t  ENAREQ                   :  8; /*!< Enable request */
      uint8_t  ENASTS                   :  8; /*!< Enable status */
      uint8_t  INVERT                   :  8;
      uint8_t  UPDATE                   :  8;
    };
    uint32_t WORD;
  } CTRL; /* +0x004 */

  union {
    struct {
      uint16_t PWIDTH                   : 16; /*!< Pulse Width */
      uint16_t PERIOD                   : 16;
    };
    uint32_t WORD;
  } PULSE; /* +0x008 */

  union {
    struct {
      uint8_t  ENABLE                   :  8; /*!< Interrupt enable */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  CLEAR                    :  8; /*!< Interrupt clear */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } INTEDGECTRL; /* +0x00C */

  union {
    struct {
      uint8_t  STATUS                   :  8; /*!< Interrupt status */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  IRQ                      :  8; /*!< Interrupt active */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } INTEDGESTATUS; /* +0x010 */

  union {
    struct {
      uint8_t  ENABLE                   :  8; /*!< Interrupt enable */
      uint8_t  CLEAR                    :  8; /*!< Interrupt clear */
      uint8_t  STATUS                   :  8; /*!< Interrupt status */
      uint8_t  IRQ                      :  8; /*!< Interrupt active */
    };
    uint32_t WORD;
  } INTUPDATED; /* +0x014 */

} PWM1_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of PWM1 SFRS.
 */
#define PWM1_SFRS ((__IO PWM1_SFRS_t *)0x50002800)

#endif /* end of __PWM1_SFR_H__ section */


