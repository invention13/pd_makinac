/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file uart_sfr.h
 */

#ifndef __UART_SFR_H__
#define __UART_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for UART.
 */
typedef struct {

  uint8_t  DATA;                              /* +0x000 */
  uint8_t  _RESERVED_01[3];                   /* +0x001 */

  union {
    struct {
      uint8_t  FRAMEERROR               :  1; /*!< Framing Error */
      uint8_t  PARITYERROR              :  1; /*!< Parity Error */
      uint8_t  BREAKERROR               :  1; /*!< Break Error */
      uint8_t                           :  5; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } UARTDATARECEIVESTATUS; /* +0x004 */

  union {
    struct {
      uint8_t  ENABLE                   :  1;
      uint8_t  ENABLE_STS               :  1; /*!< Enable status */
      uint8_t  UFIFOSOFTRESET           :  1; /*!< FIFO SOFT RESET */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t                           :  8; /*   (reserved) */
      uint8_t  SIZE                     :  2; /*!< Transmission word size */
      uint8_t  STOP                     :  1; /*!< Stop bit control */
      uint8_t  PARENA                   :  1; /*!< Parity enable */
      uint8_t  PARODD                   :  1; /*!< Odd parity */
      uint8_t  STICKENA                 :  1; /*!< Sticky partiy enable */
      uint8_t  BREAKENA                 :  1; /*!< Break enable */
      uint8_t  LOOPENA                  :  1; /*!< Loopback enable */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } MSGCTRL; /* +0x008 */

  union {
    struct {
      uint8_t  URTSENA                  :  8; /*!< RTS Enable */
      uint8_t  UCTSENA                  :  8; /*!< CTS Enable */
      uint8_t  URTSH                    :  8; /*!< RTS HIGH WATERMARK */
      uint8_t  URTSL                    :  8; /*!< RTS LOW WATERMARK */
    };
    uint32_t WORD;
  } FLOWCTRL; /* +0x00C */

  union {
    struct {
      uint8_t  DTRDY                    :  1; /*!< Rx Data ready */
      uint8_t  OVRUNERR                 :  1; /*!< Overrun error */
      uint8_t  FRMERR                   :  1; /*!< Framing error */
      uint8_t  PRTYERR                  :  1; /*!< Parity Error */
      uint8_t  BREAKIRQ                 :  1; /*!< Break IRQ */
      uint8_t  TXDONE                   :  1; /*!< Transmission is done */
      uint8_t  RXMULTIPLEXFERDONE       :  1; /*!< Multiple Receive Transactions Done */
      uint8_t  TXMULTIPLEXFERDONE       :  1; /*!< Multiple Transmit Transactions Done */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } UARTSTATUS; /* +0x010 */

  union {
    struct {
      uint8_t  INTRXDATAREADY           :  1; /*!< Rx Data ready Interrupt */
      uint8_t  INTRXOVERRUNERROR        :  1; /*!< Overrun error Interrupt */
      uint8_t  INTRXFRAMINGERROR        :  1; /*!< Framing error Interrupt */
      uint8_t  INTRXPARITYERROR         :  1; /*!< Parity Error Interrupt */
      uint8_t  INTRXBREAKERROR          :  1; /*!< Break Error Interrupt */
      uint8_t  INTTXDONE                :  1; /*!< Transmission done Interrupt */
      uint8_t  INTRXMULTIPLEXFERDONE    :  1; /*!< Multiple Receive Transactions Done Interrupt */
      uint8_t  INTTXMULTIPLEXFERDONE    :  1; /*!< Multiple Transmit Transactions Done Interrupt */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } UARTINTSTATUS; /* +0x014 */

  union {
    struct {
      uint8_t  INTRXDATAREADYENA        :  1; /*!< Rx Data ready Interrupt Enable */
      uint8_t  INTRXOVERRUNERRORENA     :  1; /*!< Overrun error Interrupt Enable */
      uint8_t  INTRXFRAMINGERRORENA     :  1; /*!< Framing error Interrupt Enable */
      uint8_t  INTRXPARITYERRORENA      :  1; /*!< Parity Error Interrupt Enable */
      uint8_t  INTRXBREAKERRORENA       :  1; /*!< Break Error Interrupt Enable */
      uint8_t  INTTXDONEENA             :  1; /*!< Transmission done Interrupt Enable */
      uint8_t  INTRXMULTIPLEXFERDONEENA :  1; /*!< Multiple Receive Transactions Done Interrupt Enable */
      uint8_t  INTTXMULTIPLEXFERDONEENA :  1; /*!< Multiple Transmit Transactions Done Interrupt Enable */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } UARTINTENABLE; /* +0x018 */

  union {
    struct {
      uint8_t  INTRXDATAREADYCLR        :  1; /*!< Rx Data ready Interrupt Clear */
      uint8_t  INTRXOVERRUNERRORCLR     :  1; /*!< Overrun error Interrupt Clear */
      uint8_t  INTRXFRAMINGERRORCLR     :  1; /*!< Framing error Interrupt Clear */
      uint8_t  INTRXPARITYERRORCLR      :  1; /*!< Parity Error Interrupt Clear */
      uint8_t  INTRXBREAKERRORCLR       :  1; /*!< Break Error Interrupt  Clear */
      uint8_t  INTTXDONECLR             :  1; /*!< Transmission done Interrupt Clear */
      uint8_t  INTRXMULTIPLEXFERDONECLR :  1; /*!< Multiple Receive Transactions Done Interrupt Clear */
      uint8_t  INTTXMULTIPLEXFERDONECLR :  1; /*!< Multiple Transmit Transactions Done Interrupt Clear */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } UARTINTCLEAR; /* +0x01C */

  union {
    struct {
      uint16_t BAUDDIV                  : 16; /*!< Baud rate divider */
      uint8_t  OSR                      :  8; /*!< Over-sampling ratio */
      uint8_t  UADVANCE                 :  1; /*!< Advance Register */
      uint8_t                           :  3; /*   (reserved) */
      uint8_t  URETARD                  :  1; /*!< Retard Register */
      uint8_t                           :  3; /*   (reserved) */
    };
    uint32_t WORD;
  } UARTBAUD; /* +0x020 */

  union {
    struct {
      uint8_t  RXUF                     :  1; /*!< Reception FIFO underflow */
      uint8_t  RXEMPTY                  :  1; /*!< Reception FIFO empty */
      uint8_t  RXFULL                   :  1; /*!< Reception FIFO empty */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  RXCOUNT                  :  8; /*!< Reception FIFO current count */
      uint8_t  TXOF                     :  1; /*!< Transmit FIFO overflow */
      uint8_t  TXEMPTY                  :  1; /*!< Transmit FIFO empty */
      uint8_t  TXFULL                   :  1; /*!< Transmit FIFO full */
      uint8_t                           :  5; /*   (reserved) */
      uint8_t  TXCOUNT                  :  8; /*!< Transmit FIFO current count */
    };
    uint32_t WORD;
  } UARTFIFOSTATUS; /* +0x024 */

  union {
    struct {
      uint8_t  RXMULTIPLEXFERDONECNT    :  8; /*!< Receive Data Count Interrupt */
      uint8_t  TXMULTIPLEXFERDONECNT    :  8; /*!< Transmit Data Count Interrupt */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } UARTFIFOLEVELCTL; /* +0x028 */

  union {
    struct {
      uint8_t  TXDMAENA                 :  8; /*!< Transmit FIFO DMA Enable.  When set to 1, DMA requests are enabled for the transmit FIFO. */
      uint8_t  RXDMAENA                 :  8; /*!< Receive FIFO DMA Enable.  When set to 1, DMA requests are enabled for the receive FIFO. */
      uint8_t  TXFIFODMAREQLVL          :  8; /*!< Transmit FIFO Watermark */
      uint8_t  RXFIFODMAREQLVL          :  8; /*!< Receive FIFO Watermark */
    };
    uint32_t WORD;
  } UARTDMACTL; /* +0x02C */

} UART_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of UART SFRS.
 */
#define UART_SFRS ((__IO UART_SFRS_t *)0x50004800)

#endif /* end of __UART_SFR_H__ section */


