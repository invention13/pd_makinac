/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file i2c_sfr.h
 */

#ifndef __I2C_SFR_H__
#define __I2C_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for I2C.
 */
typedef struct {

  union {
    struct {
      uint8_t  BUFOVFL                  :  1; /*!< Data / address received (slave mode) */
      uint8_t  BUFINVALID               :  1; /*!< Write buffer overflow */
      uint8_t  BUFFULL                  :  1; /*!< Buffer full */
      uint8_t  R1W0                     :  1; /*!< Read / write busy */
      uint8_t  PRCVD                    :  1; /*!< Stop bit received */
      uint8_t  SRCVD                    :  1; /*!< Start bit received */
      uint8_t  ADRRCVD                  :  1; /*!< Data / address received (slave mode) */
      uint8_t  ACKNRCVD                 :  1; /*!< ACK received */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } STATUS; /* +0x000 */

  union {
    struct {
      uint8_t  RSENA                    :  1; /*!< Repeated start bit (master only) */
      uint8_t  STRHCLK                  :  1; /*!< Clock stretch (slave only) */
      uint8_t  GCENA                    :  1; /*!< General call address enable (slave only) */
      uint8_t  MRCVENA                  :  1; /*!< Start bit reception (master only and cleared by HW) */
      uint8_t  ACKN                     :  1; /*!< ACK bit to be transmtted (master only) */
      uint8_t  ACKENA                   :  1; /*!< ACK bit (master only) */
      uint8_t  PENA                     :  1; /*!< Stop bit or selection of address size */
      uint8_t  SENA                     :  1; /*!< Start and stretch */
      uint8_t  M1S0                     :  1; /*!< I2C master / slave */
      uint8_t  FLTENA                   :  1; /*!< I2C filter enable */
      uint8_t  ENA_REQ                  :  1; /*!< I2C enable Request */
      uint8_t  ENA_STS                  :  1; /*!< I2C enable status */
      uint8_t                           :  4; /*   (reserved) */
      uint8_t  AMSK                     :  8; /*!< I2C address mask (slave only) */
      uint8_t                           :  8; /*   (reserved) */
    };
    uint32_t WORD;
  } CTRL; /* +0x004 */

  uint8_t  I2CDATA;                           /*<! I2C Data +0x008 */
  uint8_t  _RESERVED_09[3];                   /* +0x009 */

  uint16_t I2CADDR;                           /*<! I2C Address +0x00C */
  uint8_t  _RESERVED_0E[2];                   /* +0x00E */

} I2C_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of I2C SFRS.
 */
#define I2C_SFRS ((__IO I2C_SFRS_t *)0x50004c00)

#endif /* end of __I2C_SFR_H__ section */


