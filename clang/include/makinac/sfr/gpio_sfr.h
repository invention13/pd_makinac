/**
 * @copyright 2020 indie Semiconductor
 *
 * This file is proprietary to indie Semiconductor.
 * All rights reserved. Reproduction or distribution, in whole
 * or in part, is forbidden except by express written permission
 * of indie Semiconductor.
 *
 * @file gpio_sfr.h
 */

#ifndef __GPIO_SFR_H__
#define __GPIO_SFR_H__

#include <stdint.h>

/* -------  Start of section using anonymous unions and disabling warnings  ------- */
#if   defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief A structure to represent Special Function Registers for GPIO.
 */
typedef struct {

  uint8_t  GPADATA[1024];                     /*<! Port A data +0x000 */

  uint8_t  GPBDATA[1024];                     /*<! Port B data +0x400 */

  union {
    struct {
      uint8_t  GPAENA                   :  1;
      uint8_t  GPBENA                   :  1;
      uint8_t                           :  6; /*   (reserved) */
      uint32_t                          : 24; /*   (reserved) */
    };
    uint32_t WORD;
  } GPENA; /* +0x800 */

  union {
    struct {
      uint8_t  GPADIR0                  :  1; /*!< Pin 0 output enable */
      uint8_t  GPAIE0                   :  1; /*!< Pin 0 interrupt mask */
      uint8_t  GPARE0                   :  1; /*!< Pin 0 rising edge enable */
      uint8_t  GPAFE0                   :  1; /*!< Pin 0 falling edge enable */
      uint8_t  GPACLR0                  :  1; /*!< Pin 0 interrupt clear */
      uint8_t  GPAACTDET0               :  1; /*!< Pin 0 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR1                  :  1; /*!< Pin 1 output enable */
      uint8_t  GPAIE1                   :  1; /*!< Pin 1 interrupt mask */
      uint8_t  GPARE1                   :  1; /*!< Pin 1 rising edge enable */
      uint8_t  GPAFE1                   :  1; /*!< Pin 1 falling edge enable */
      uint8_t  GPACLR1                  :  1; /*!< Pin 1 interrupt clear */
      uint8_t  GPAACTDET1               :  1; /*!< Pin 1 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR2                  :  1; /*!< Pin 2 output enable */
      uint8_t  GPAIE2                   :  1; /*!< Pin 2 interrupt mask */
      uint8_t  GPARE2                   :  1; /*!< Pin 2 rising edge enable */
      uint8_t  GPAFE2                   :  1; /*!< Pin 2 falling edge enable */
      uint8_t  GPACLR2                  :  1; /*!< Pin 2 interrupt clear */
      uint8_t  GPAACTDET2               :  1; /*!< Pin 2 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR3                  :  1; /*!< Pin 3 output enable */
      uint8_t  GPAIE3                   :  1; /*!< Pin 3 interrupt mask */
      uint8_t  GPARE3                   :  1; /*!< Pin 3 rising edge enable */
      uint8_t  GPAFE3                   :  1; /*!< Pin 3 falling edge enable */
      uint8_t  GPACLR3                  :  1; /*!< Pin 3 interrupt clear */
      uint8_t  GPAACTDET3               :  1; /*!< Pin 3 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPAP03; /* +0x804 */

  union {
    struct {
      uint8_t  GPADIR4                  :  1; /*!< Pin 4 output enable */
      uint8_t  GPAIE4                   :  1; /*!< Pin 4 interrupt mask */
      uint8_t  GPARE4                   :  1; /*!< Pin 4 rising edge enable */
      uint8_t  GPAFE4                   :  1; /*!< Pin 4 falling edge enable */
      uint8_t  GPACLR4                  :  1; /*!< Pin 4 interrupt clear */
      uint8_t  GPAACTDET4               :  1; /*!< Pin 4 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPADIR5                  :  1; /*!< Pin 5 output enable */
      uint8_t  GPAIE5                   :  1; /*!< Pin 5 interrupt mask */
      uint8_t  GPARE5                   :  1; /*!< Pin 5 rising edge enable */
      uint8_t  GPAFE5                   :  1; /*!< Pin 5 falling edge enable */
      uint8_t  GPACLR5                  :  1; /*!< Pin 5 interrupt clear */
      uint8_t  GPAACTDET5               :  1; /*!< Pin 5 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint16_t                          : 16; /*   (reserved) */
    };
    uint32_t WORD;
  } GPAP47; /* +0x808 */

  union {
    struct {
      uint8_t  GPBDIR0                  :  1; /*!< Pin 0 output enable */
      uint8_t  GPBIE0                   :  1; /*!< Pin 0 interrupt mask */
      uint8_t  GPBRE0                   :  1; /*!< Pin 0 rising edge enable */
      uint8_t  GPBFE0                   :  1; /*!< Pin 0 falling edge enable */
      uint8_t  GPBCLR0                  :  1; /*!< Pin 0 interrupt clear */
      uint8_t  GPBACTDET0               :  1; /*!< Pin 0 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR1                  :  1; /*!< Pin 1 output enable */
      uint8_t  GPBIE1                   :  1; /*!< Pin 1 interrupt mask */
      uint8_t  GPBRE1                   :  1; /*!< Pin 1 rising edge enable */
      uint8_t  GPBFE1                   :  1; /*!< Pin 1 falling edge enable */
      uint8_t  GPBCLR1                  :  1; /*!< Pin 1 interrupt clear */
      uint8_t  GPBACTDET1               :  1; /*!< Pin 1 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR2                  :  1; /*!< Pin 2 output enable */
      uint8_t  GPBIE2                   :  1; /*!< Pin 2 interrupt mask */
      uint8_t  GPBRE2                   :  1; /*!< Pin 2 rising edge enable */
      uint8_t  GPBFE2                   :  1; /*!< Pin 2 falling edge enable */
      uint8_t  GPBCLR2                  :  1; /*!< Pin 2 interrupt clear */
      uint8_t  GPBACTDET2               :  1; /*!< Pin 2 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
      uint8_t  GPBDIR3                  :  1; /*!< Pin 3 output enable */
      uint8_t  GPBIE3                   :  1; /*!< Pin 3 interrupt mask */
      uint8_t  GPBRE3                   :  1; /*!< Pin 3 rising edge enable */
      uint8_t  GPBFE3                   :  1; /*!< Pin 3 falling edge enable */
      uint8_t  GPBCLR3                  :  1; /*!< Pin 3 interrupt clear */
      uint8_t  GPBACTDET3               :  1; /*!< Pin 3 activity interrupt */
      uint8_t                           :  2; /*   (reserved) */
    };
    uint32_t WORD;
  } GPBP03; /* +0x80C */

} GPIO_SFRS_t;

/* --------  End of section using anonymous unions and disabling warnings  -------- */
#if   defined (__CC_ARM)
  #pragma pop
#elif defined (__ICCARM__)
  /* leave anonymous unions enabled */
#elif (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic pop
#elif defined (__GNUC__)
  /* anonymous unions are enabled by default */
#elif defined (__TMS470__)
  /* anonymous unions are enabled by default */
#elif defined (__TASKING__)
  #pragma warning restore
#elif defined (__CSMC__)
  /* anonymous unions are enabled by default */
#else
  #warning Not supported compiler type
#endif

/**
 * @brief The starting address of GPIO SFRS.
 */
#define GPIO_SFRS ((__IO GPIO_SFRS_t *)0x50001000)

#endif /* end of __GPIO_SFR_H__ section */


