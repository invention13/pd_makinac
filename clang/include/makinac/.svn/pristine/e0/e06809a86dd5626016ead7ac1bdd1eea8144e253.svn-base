#ifndef __MACKINAC_H__
#define __MACKINAC_H__

// declare an enumerated type that can be used to refer to interrupts by name, e.g. in calls to the NVIC configuration
// functions - the definitions here must match the vector table positions, offset so that IRQ0 (the 1st ASIC IRQ) has
// the value 0...
//
typedef enum IRQn
{
  // Cortex-M0 core exceptions...
           NMI_IRQn = -14,
         Reset_IRQn = -15,
     HardFault_IRQn = -13,
           SVC_IRQn = -5,
        PendSV_IRQn = -2,
       SysTick_IRQn = -1,
  // QIN-specific IRQs... (should match the vector defined in dig_qin_top.sv)
        ClockFault_IRQn = 0,
          Brownout_IRQn = 1,
        Watchdog_A_IRQn = 2,
              GPIO_IRQn = 3,
            Offset_IRQn = 4,
        LIN_Master_IRQn = 5,
        LIN_Wakeup_IRQn = 6,
         LIN_Slave_IRQn = 7,
             PWM_0_IRQn = 8,
             PWM_1_IRQn = 9,
          Timer_L_IRQn = 10,
          Timer_M_IRQn = 11,
            UART_A_IRQn = 12,
             ADC_A_IRQn = 13,
              VBAT_IRQn = 14,
           Lullaby_IRQn = 15,
  // Clough peripheral IRQs...
        Timer0_IRQn =  16,
        Timer1_IRQn =  17,
        Timer2_IRQn =  18,
      Watchdog_IRQn =  19,
           BTE_IRQn =  20,
          SDIO_IRQn =  21
} IRQn_Type;
//
// and define a tell-tale macro that will prevent the clough.h header from attempting to re-define this with the
// default (non-ASIC-specific) version...
//
#define __IRQn_Type

#include <stdint.h>
#include "mackinac_sfr.h"
#include "verne.h"

// things that should be auto-generated in the SFR headers?

#define CLOCKSRC_HFCLKSEL_RC    0
#define CLOCKSRC_HFCLKSEL_XO    1
#define CLOCKSRC_SYSCLKSEL_LF   0
#define CLOCKSRC_SYSCLKSEL_HF   1

#define RESETCTRL_POR           1
#define RESETCTRL_BOR_3V3       2
#define RESETCTRL_BOR_2V6       4
#define RESETCTRL_BOR_1V8       8
#define RESETCTRL_WDTA          16

#define PMUA_CTRL_HIBERNATE     1
#define PMUA_CTRL_FASTBOOT      2
#define PMUA_CTRL_FASTSHUTDOWN  4

#define WDTA_STOP_CODE 0x89abcdef //magic value that stops the watchdog 0x6da475c3

#define F_CPU_HZ 16000000U

#endif


