#include "states.h"

void *state_2( PD_Port_t *p_Context, Event_t *event )
{
	switch( event->Event_ID )
	{
	case ENTRANCE:
                v_Start_Timer( p_Context, 2000 );
                v_Set_Debug_GPIO( GPIO10_DEBUG, 0);
		return (void *)0;
		break;

	case EXIT:
		return (void *)0;
		break;
                
        case TIMEOUT_EVENT:
                return (void *)state_1;
                break;

	default:
		break;
	}
	return (void *)state_2;
}