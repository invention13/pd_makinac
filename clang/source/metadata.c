/** @file metadata.c
 *  @brief Populate metadata section.
 *
 *  Version information for metadata. This is so that the version information
 *  can be read at a standard address - 0x27700 - using an attached j-link  device.
 *  
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */

#include <stdint.h>
#include "version.h"

struct Metadata
{
    uint8_t u8_Version_Build;
    uint8_t u8_Version_Fix;
    uint8_t u8_Version_Minor;
    uint8_t u8_Version_Major;
};

#pragma location = "METADATA"
const struct Metadata metadata = {  BUILD_VERSION, FIX_VERSION, MINOR_VERSION, MAJOR_VERSION };


