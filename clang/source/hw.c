/** @file hw.c
 *  @brief HW initialization
 *
 *  Set up hardware and program trim values.
 *  
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */

#include <stdint.h>
#include "superior/superior.h"
#include "hw.h"
#include "cal.h"

uint32_t SystemCoreClock = 4000000;

/** @brief System Initialization
 *        
 *
 *  Set up clocks and trims.
 *
 *  @param None
 *  @return Void.
 */
void SystemInit(void)
{  
 // Disable watchdog
  WDTERBIUM_SFRS->COMMAND = 0x89ABCDEF;
  
  // Basic trims like bandgap voltage, low speed oscillator
  v_Basic_Trims_From_Flash();
  
  // PLL Calibration
  v_PLL_Calibration( );
  
  // Default CDR Setup
  v_Default_CDR_Setup();
  
  // Get trims from flash and write to trim memory
  v_Apply_Trims_From_Flash();
  
}

/** @brief Reads byte from flash.
 *        
 *
 *  Reads a byte from flash. Used to retrieve from non-volatile cal memory.
 *
 *  @param None
 *  @return Void.
 */
static uint8_t u8_Fetch_NV_Byte( uint32_t u32_Address )
{
  volatile uint8_t *ptr = (volatile uint8_t *)u32_Address;
  
  return *ptr;
}

void v_Get_Chip_Info( Chip_Info_t *p_Info )
{
  p_Info->u32_Timestamp = u8_Fetch_NV_Byte( 0x27f00 ) | 
                          u8_Fetch_NV_Byte( 0x27f01 ) << 8 | 
                          u8_Fetch_NV_Byte( 0x27f02 ) << 16 |
                          (u8_Fetch_NV_Byte( 0x27f03 ) & 0xf) << 24;
  
  p_Info->u8_ATE_Loadboard_Number = u8_Fetch_NV_Byte( 0x27f03 ) >> 4;
  p_Info->u16_Lot_Number = u8_Fetch_NV_Byte( 0x27f04 ) << 8 | u8_Fetch_NV_Byte( 0x27f05 );
  p_Info->u8_Chip_Revision = u8_Fetch_NV_Byte( 0x27f06 );
  p_Info->u8_Chip_Name = u8_Fetch_NV_Byte( 0x27f07 );
  p_Info->u8_Cal_Version = u8_Fetch_NV_Byte( 0x27f08 );
  p_Info->u32_ATE_Program_Date_Code = u8_Fetch_NV_Byte( 0x27f40 ) | 
                                      u8_Fetch_NV_Byte( 0x27f41 ) << 8 | 
                                      u8_Fetch_NV_Byte( 0x27f42 ) << 16;
  
  p_Info->u8_ATE_Site_Number = u8_Fetch_NV_Byte( 0x27f43 );
}



void dbgTrapErr( void ) // TODO: Need to provide a better definition for this
{
  while(1);
}

void v_Delay_us( uint8_t u8_Delay )
{
  volatile uint32_t u32_Dummy;
  uint32_t u32_Timeout = (uint32_t)u8_Delay * 20;
  
  for( u32_Dummy = 0; u32_Dummy < u32_Timeout; u32_Dummy++ );
}
