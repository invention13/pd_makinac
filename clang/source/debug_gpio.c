#include "superior/superior.h"
#include "superior/sfr/nvmtrim.h"

#include "debug_gpio.h"

#if defined( USE_DEBUG_GPIOS ) && !defined( NDEBUG )
#pragma message("***J1206 GPIOs in use for debug***")
#endif

/** @brief Initializes use of SD card related GPIOs for debug
 *        
 *
 *  For use with the Superior eval board. Can use the SD card related
 *  GPIOs on J1206 for debug to observe events or timing.
 *  
 *  @param[in] args A pointer to structure containing arguments to initialize hub task.
 *  @return Void.
 */
void _v_Init_Debug_GPIO(void)
{
   // Taken from APP note
  TRIMMEMORY_SFRS->TRIMDATA0.CARDPOWERFILTEN = 0;
  TRIMMEMORY_SFRS->TRIMDATA0.CARDPOWERFWVAL = 0;
  TRIMMEMORY_SFRS->TRIMDATA0.CARDPOWERFWCTL = 1;
  TRIMMEMORY_SFRS->TRIMDATA0.CARDPOWEREN = 0;

  TRIMMEMORY_SFRS->TRIMDATA0.OCP3P3NFW = 0;
  TRIMMEMORY_SFRS->TRIMDATA0.O3P31P8NFW = 0;
  TRIMMEMORY_SFRS->TRIMDATA0.OCP3P3N1P8FW = 0;
  TRIMMEMORY_SFRS->TRIMDATA0.OGNDNFW3V = 1;
  TRIMMEMORY_SFRS->TRIMDATA0.Q0BIT0 = 1;
  TRIMMEMORY_SFRS->TRIMDATA0.Q0BIT1 = 1;
  
   // GPIO10
  IOCTRL_SFRS->GPIO10CTRL.GP10MODE = 0;
  IOCTRL_SFRS->GPIO10CTRL.GP10OE = 1;
  GPIO_SFRS->GPBP03.GPBDIR1 = 1; 
  
  // GPIO11
  IOCTRL_SFRS->GPIO11CTRL.GP11MODE = 0;
  IOCTRL_SFRS->GPIO11CTRL.GP11OE = 1;
  GPIO_SFRS->GPBP03.GPBDIR2 = 1; 
  
  // GPIO12
  IOCTRL_SFRS->GPIO12CTRL.GP12MODE = 0;
  IOCTRL_SFRS->GPIO12CTRL.GP12OE = 1;
  GPIO_SFRS->GPBP03.GPBDIR3 = 1; 
  
  // GPIO13
  IOCTRL_SFRS->GPIO13CTRL.GP13MODE = 0;
  IOCTRL_SFRS->GPIO13CTRL.GP13OE = 1;
  GPIO_SFRS->GPBP47.GPBDIR4 = 1;
  
  // GPIO14
  IOCTRL_SFRS->GPIO14CTRL.GP14MODE = 0;
  IOCTRL_SFRS->GPIO14CTRL.GP14OE = 1;
  GPIO_SFRS->GPBP47.GPBDIR5 = 1;
  
  // GPIO15
  IOCTRL_SFRS->GPIO15CTRL.GP15MODE = 0;
  IOCTRL_SFRS->GPIO15CTRL.GP15OE = 1;
  GPIO_SFRS->GPBP47.GPBDIR6 = 1;
  
  // Initially clear all of the lines
  v_Set_Debug_GPIO( 0, DEBUG_GPIOS );
 
}

/** @brief Set and clear GPIOs on J1206
 *        
 *
 *  For use with the Superior eval board. Can use the SD card related
 *  GPIOs on J1206 for debug to observe events or timing. If a GPIO is listed
 *  in both set and clear, a short (3-4 uS) pulse is generated (when 30 MHz clock is used).
 *  
 *  @param[in] u8_Set Bitmask for GPIOs to set
 *  @param[in] u8_Clear Bitmask for GPIOs to clear
 *
 *  @return Void.
 */
void _v_Set_Debug_GPIO( uint8_t u8_Set, uint8_t u8_Clear )
{
  volatile uint32_t *pu32_Base_Address = (uint32_t *)GPIO_SFRS->GPBDATA;
  volatile uint32_t *pu32_Set_Address, *pu32_Clear_Address;
  
  // Set
  if( DEBUG_GPIOS & u8_Set )
  {
    pu32_Set_Address = pu32_Base_Address + u8_Set; // Shift the address for mask
    *pu32_Set_Address = u8_Set;
  }
  
  // Clear
  if( DEBUG_GPIOS & u8_Clear )
  {
    pu32_Clear_Address = pu32_Base_Address + u8_Clear; // Shift the address for mask
    *pu32_Clear_Address = ~u8_Clear;
  }
  
}
