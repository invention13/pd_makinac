/** @file main.c
 *  @brief main function for hub
 *
 * This initializes the board hardware and the hub.
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */

#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "debug_gpio.h"

#include "hw.h"
   
#include "PD_Port.h"
   
PD_Port_Init_t Port1_Init = { .TypeC_Base = (volatile uint32_t *)TYPEC1_SFRS, 
                              .u8_Name = 0,
                              .e_Power_Role = PORT_ROLE_SOURCE };

PD_Port_Init_t Port2_Init = { .TypeC_Base = (volatile uint32_t *)TYPEC2_SFRS, 
                              .u8_Name = 0,
                              .e_Power_Role = PORT_ROLE_SOURCE };

static PD_Port_t Port1;
static PD_Port_t Port2;

//void v_Init_Debug_GPIO(void);
void v_Test_Task(void *args );
const uint16_t u16_Test_Task_Stack_Size = 128;

/** @brief Main function
 *        
 *
 *  Initializes board and usb ports, launches hub task
 *
 *  @param None
 *  @return Error Status
 */
int main()
{
  
  SystemInit();
  
  //v_Init_Debug_GPIO();
  
  Port1_Init.u8_Name = "Port1";
  v_Init_PD_Port( &Port1_Init, &Port1  );
  
  Port2_Init.u8_Name = "Port2";
  v_Init_PD_Port( &Port2_Init, &Port2  );
  
//  xTaskCreate( v_Test_Task,			
//            "Test Task", 		        
//            u16_Test_Task_Stack_Size,          
//            &Port1, 			       
//            tskIDLE_PRIORITY, 		
//            NULL );
  
  vTaskStartScheduler();  // This function doesn't return
  
  return -1;              // Should not reach this point.
}
