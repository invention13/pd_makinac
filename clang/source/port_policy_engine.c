#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include <string.h>
#include "events.h"
#include "port_policy_engine.h"

#include "PD_Port.h"
#include "debug_gpio.h"

void v_Update_PE_State( void *p_Owning_Port, Event_t *event ); 

void v_Init_Port_Policy_Engine( void *p_Owning_Port, void *p_Init_Args )
{
  PD_Port_Init_t *p_Init = (PD_Port_Init_t *)p_Init_Args;
  PD_Port_t *p_Port = (PD_Port_t *)p_Owning_Port;
  
   // Event queue
   p_Port->Port_Policy_Engine.Event_Queue = xQueueCreate( PD_EVENT_QUEUE_LENGTH, sizeof(Event_t) );
   if( p_Port->Port_Policy_Engine.Event_Queue == NULL )
   { // Error condition, can't create queue - TODO: add error handler here
   }
   
   xTaskCreate(  v_Port_Policy_Engine,
                "Port Policy Engine",
                PORT_POLICY_ENGINE_STACK_SIZE,
                p_Port,   // reference back to port that owns task
                tskIDLE_PRIORITY,
                &p_Port->PPE_Task );
}

// Multiple copies of this thread may exist - must be re-entrant
void v_Port_Policy_Engine( void *args )
{
#pragma diag_suppress = Pe177
    PD_Port_t *self = (PD_Port_t *)args;
    Event_t Event;
    uint32_t u32_Msg_Cnt = 0;
    
    v_Announce_Ready( self, POLICY_ENGINE_READY );
    v_Wait_Until_Ready( self, ALL_READY );

     while(1)
     {
       // Block indefinitely while waiting for events
       if( xQueueReceive( self->Port_Policy_Engine.Event_Queue, &Event, portMAX_DELAY ) )
       {
           // Check for DPM requests and other events that are not state specific
           v_Update_PE_State( self, &Event );
           ++u32_Msg_Cnt;
       }
     }
 }

void v_Post_PE_Event( void *p_Owning_Port, Event_t *p_Event )
{
    PD_Port_t *p_Port = (PD_Port_t *)p_Owning_Port;
    xQueueSend( p_Port->Port_Policy_Engine.Event_Queue, ( void * )p_Event, ( TickType_t ) 0 );
}