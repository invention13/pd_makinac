#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include <string.h>
#include "events.h"
#include "event_groups.h"
#include "hw.h"
#include "PD_Port.h"
#include "debug_gpio.h"
#include "phy.h"

typedef enum
{
  PHY_RESET,
  INITIALIZE

} e_PHY_Command_t;

void *pv_PHY_Control( e_PHY_Command_t e_Command, PD_Port_t *port,  void *arg)
{
 volatile TYPEC1_SFRS_t* p_Typec = (volatile TYPEC1_SFRS_t *)port->TypeC_Base;
 
    switch( e_Command )
    {
    case PHY_RESET:
      p_Typec->POWER.WORD = EN_AFE;
      v_Delay_us( 10 );
      
      p_Typec->TYPECCTL3.WORD = RST_PHY;
      v_Delay_us( 10 );
        
      break;
      
    case INITIALIZE:
      if( port->e_Port_Role == PORT_ROLE_SOURCE )
      {
           p_Typec->TYPECCTL1.WORD = port->Rp | MODE_SRC_STAY;
           p_Typec->PDCFG2.WORD = CDR_VERSION | RX_SOP0 | RX_SOP1;
           p_Typec->PDCFG3.WORD = 0x04 | PR_PROVIDER | DR_DFP;
      }
      else if( port->e_Port_Role == PORT_ROLE_SINK )
      {
        p_Typec->TYPECCTL1.WORD = MODE_SNK;
        p_Typec->PDCFG2.WORD = CDR_VERSION | RX_SOP0;
        p_Typec->PDCFG3.WORD = 0x04 | PR_CONSUMER | DR_UFP;
      }
      else
      {
      }
      
             
    default:
      break;
    }
    
    return (void *)0;
}