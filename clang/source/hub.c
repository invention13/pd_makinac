/** @file hub.c
 *  @brief Initialization and operation of hub
 *
 *  Set up hub and port hardware, the port change interrupt.
 *  
 *
 *  @author Ian Board
 *  @bug No known bugs.
 */

#include "superior.h"
#include "FreeRTOS.h"
#include "task.h"
#include "usbhub_descript.h" 
#include "debug_gpio.h"

/** @brief Set up GPIOs for USB ports
 *        
 *
 *  Use FAULT_NX mode where pull-up and pull-downs are driven by PPC
 *
 *  @param None
 *  @return Void.
 */
static void v_Init_Hub_GPIOs(void)
{

  // IOCTRL:  FAULTN1CTRL:  FAULTN1RE=1,  FAULTN1MODE=1 (FAULT_N1 Mode)
  IOCTRL_SFRS->FAULTN1CTRL.FAULTN1RE = 0x1;
  IOCTRL_SFRS->FAULTN1CTRL.FAULTN1MODE = 0x1;
    
  // IOCTRL:  PRTCTL1CTRL:  PRTCTL1MODE=1 (PRTCTL_1 Mode), PRTCTL1SEL=1 (DATA from 1.5 DigitalCore)
  // *NOTE*  PRTCTL Output Enable is enabled by default and is controlled via a register in the Trim Memory
  IOCTRL_SFRS->PRTCTL1CTRL.PRTCTL1MODE = 0x1;
  IOCTRL_SFRS->PRTCTL1CTRL.PRTCTL1SEL = 0x1;

  // IOCTRL:  FAULTN2CTRL:  FAULTN2RE=1,  FAULTN2MODE=1 (FAULT_N2 Mode)
  IOCTRL_SFRS->FAULTN2CTRL.FAULTN2RE = 0x1;
  IOCTRL_SFRS->FAULTN2CTRL.FAULTN2MODE = 0x1;
    
  // IOCTRL:  PRTCTL2CTRL:  PRTCTL2MODE=1 (PRTCTL_2 Mode), PRTCTL2SEL=1 (DATA from 1.5 DigitalCore)
  // *NOTE*  PRTCTL Output Enable is enabled by default and is controlled via a register in the Trim Memory
  IOCTRL_SFRS->PRTCTL2CTRL.PRTCTL2MODE = 0x1;
  IOCTRL_SFRS->PRTCTL2CTRL.PRTCTL2SEL = 0x1; 

  // IOCTRL:  GPIO3CTRL:    GP3RE=1,  GP3MODE=1 (FAULT_N3 Mode)
  IOCTRL_SFRS->GPIO3CTRL.GP3RE = 0x1;
  IOCTRL_SFRS->GPIO3CTRL.GP3MODE = 0x1;
       
  // IOCTRL:  GPIO2CTRL:    GP2MODE=1 (PRTCTL_3 Mode), PRTCTL3SEL=1 (DATA from 1.5 DigitalCore)
  // *NOTE*  PRTCTL Output Enable is enabled by default and is controlled via a register in the Trim Memory
  IOCTRL_SFRS->GPIO2CTRL.GP2MODE = 0x1;
  IOCTRL_SFRS->GPIO2CTRL.PRTCTL3SEL = 0x1;
}

/** @brief Set up Port Power Control (PPC)
 *        
 *
 *  Configured to HUB mode with overcurrent events. All ports are driven by
 *  raw fault pad signal.
 *
 *  @param None
 *  @return Void.
 */
static void v_Init_PPC(void)
{

  // PPC: Enable the Pull-Up on the FAULT_N# pads.  
  // PPC: DFP1DRVVBUSCTRL - Put into HUB Mode
  // PPC: DFP1OCCTRL - Overcurrent events driven by FAULT PAD input
  // Overcurrent events are assumed to be active-low 0x50000035 = 0xE
  PPC_SFRS->FAULTNPUPDCTRL.BYTE = 0x0E;
    
  // Downstream Facing Port 1
  PPC_SFRS->DFP1DRVVBUSCTRL.BYTE = 0x01;   
  PPC_SFRS->DFP1OCCTRL.BYTE = 0x4;  
  
  // Downstream Facing Port 2
  PPC_SFRS->DFP2DRVVBUSCTRL.BYTE = 0x01;     
  PPC_SFRS->DFP2OCCTRL.BYTE = 0x4;
  
  // Downstream Facing Port 3
  PPC_SFRS->DFP3DRVVBUSCTRL.BYTE = 0x01;     
  PPC_SFRS->DFP3OCCTRL.BYTE = 0x4;

}

/** @brief Set up port change interrupt
 *        
 *
 *  Set up the port change interrupt if it is to be used. This sets the causes,
 *  and the priority. 
 *  
 *
 *  @param None
 *  @return Void.
 */
static void v_Init_Port_Change_IRQ(void)
{
  NVIC_DisableIRQ(PortChng_IRQn);
  _USBHUB->PORTSTCHG.WORD = 0x80066600;     
  
  //Clear and enable port change interrupt. Lower priority than TYPE-C if used
  NVIC_SetPriority(PortChng_IRQn, 3);
  NVIC_ClearPendingIRQ(PortChng_IRQn);
  NVIC_EnableIRQ(PortChng_IRQn);
}

/** @brief Initialize the hub
 *        
 *
 *  Initializes the USB hub.
 *  
 *
 *  @param None
 *  @return Void.
 */
void v_Init_Hub(void)
{
    // Initialize pins 
    v_Init_Hub_GPIOs();
    
    // Write descriptor
    USBHUB_DESCRIPT_writeDescriptor(1, 1);  

    // Initialize port power control and over-current
    v_Init_PPC();
  
  // Enable AFE for all ports AFEENA=1 BIASENA=1 also sets OVDENA, OVDAUTO, SE0AFEDGLTCH
  // Upstream facing port
   USBAFE_enableAFE( 0 , 0 ); 
  
  // Downstream facing port 1
  USBAFE_enableAFE( 1 , 0 );
  
  //Downstream facing port 2
  USBAFE_enableAFE( 2 , 0 );
  
  //Downstream facing port 3
  USBAFE_enableAFE( 3 , 0 );
    
  //Reset Configuration:
  //Take HUB out of reset to enable normal operation
  //USBHUBSFRS:  POR_CTRL:  PLL_POR=0, UPVBUS_DET_DISABLE=0,
  
   // Enable the port change interrupt if used.
   v_Init_Port_Change_IRQ();

  _USBHUB->PORCTRL.BYTE = 0x0; 

}

/** @brief Port change interrupt handler
 *        
 *
 *  The port change interrupt handler if used. This is bare-bones and is meant to be added
 *  to. This function must not be re-named since it overrides a default handler with weak
 *  linkage.
 *  
 *
 *  @param None
 *  @return Void.
 */

static uint32_t u32_Irq_Cnt;

void PortChng_Handler(void) 
{ 
    _USBHUB->PORTSTCHG.PORT1 = 1;
    _USBHUB->PORTSTCHG.PORT2 = 1;
    _USBHUB->PORTSTCHG.PORT3 = 1;    
    
    ++u32_Irq_Cnt;

   v_Set_Debug_GPIO( GPIO10_DEBUG, GPIO10_DEBUG );
}


