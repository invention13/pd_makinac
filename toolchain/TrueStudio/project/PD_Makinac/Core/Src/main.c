#include "hal.h"
#include "FreeRTOS.h"
#include "task.h"

static void v_Test_Task(void*);

int main(void)
{
  init_hal();

  xTaskCreate(     v_Test_Task,
                  "Test",
                  configMINIMAL_STACK_SIZE,
                  NULL,
				  configMAX_PRIORITIES - 1,
                  NULL );

  vTaskStartScheduler(); // this doesn't return

  while(1);

  return 1;

}

static void v_Test_Task(void *args )
{
	static uint8_t led_state;

	while(1)
	{
		vTaskDelay( 1000 );
		set_debug_gpio( 0, led_state );
		led_state = !led_state;
	}
}
