
#ifndef __gpio_H
#define __gpio_H

#include "main.h"

void MX_GPIO_Init(void);
void set_debug_gpio( uint8_t u8_debug_pin, uint8_t b_enable );
void set_LED( uint8_t b_enable );

#endif /*__ pinoutConfig_H */

