/*
 * hal.h
 *
 *  Created on: Feb 25, 2020
 *      Author: ian
 */

#ifndef HAL_H_
#define HAL_H_

#include "main.h"
#include "gpio.h"
#include "stm32l0xx_it.h"

void init_hal(void);
void SystemClock_Config(void);
void Error_Handler(void);
void assert_failed(uint8_t *file, uint32_t line);



#endif /* HAL_H_ */
