/*____________________________________________________________________________

COPYRIGHT 2010 AyDeeKay LLC - All Rights Reserved
No part of this document can be used, copied, transmitted, or modified by any
existing or to be invented means without express approval of AyDeeKay LLC.
_______________________________________________________________________________

FILE:     FlashLoader_HeimdallSlave.MAC
PROJECT:  HeimdallSlave

Description:  This file contains the macro routines to initialize the flash 
              during flashloader operation.  These routines run on the host
			  system (the IDE).

Environment Information and Building Instructions:

Hardware Platform:  
Processor:          Cortex M0

Remarks: 

REVISION HISTORY OF THE FILE

Version     Date:       By:             Description:
V0.01.00    8/16/2010   Caio Gubel      Initial Version
V0.02.00    5/3/2011    Scott Kee       Modifications to allow for max. speed
V0.02.01    5/4/2011    Caio Gubel      Adding comments/polishing the code, nothing substantial


TESTING HISTORY OF THE FILE

Version Date:     By:         Description:

******************************************************************************/
	      

/*******************************************************************************
execUserFlashInit:
Called once before the flash loader is downloaded to RAM.
Implement this macro typically for setting up the memory map
required by the flash loader. This macro is only called when you are
programming flash, and it should only be used for flash loader
functionality.
*******************************************************************************/
execUserFlashInit()  // Called by debugger before loading flash loader in RAM.
{

    __delay (1000);  
  // Tell the user what function is running
  __message " ***** execUserFlashInit() ***** ";
  
  // Set JLINK speed to 10kHz to allow for initial communication with HeimdallSlave
  // as from reset it starts slowly (~10KHz)
  __emulatorSpeed(10000);

  //Sequence to properly power-up the part's flash 
  //__message "***** Power Up Sequence Start: xtal, RC and PIR enabled! *****";
  //__writeMemory32(0x00007700, 0x50000014, "Memory");	//Address 0x50000015:                   //CG: 00007700
                                                        //Power up delay [1:0] = 11
                                                        //xtal enable[2] = enable
                                                        //auto xtal enable[3] = disable
                                                        //RC osc. enable [4] = enable
                                                        //Active low sleep enable[5] = disable
                                                        //PIR enable[6] = enabled
                                                        //SW uC reset[7] = disabled

  __message "***** Trim Memory /2, No divider, xtal selected! *****";
  // Set flash read cycles to 1
  // ATTENTION: The current flash being used has a limit access speed 
  // of ~15MHz, therefore accessing it with no wait-state @30MHz is an issue. 
  // For systems running at frequencies higher then 15MHZ the 
  // insertion of wait-states is mandatory. Trim Memory clock divider mandatory.
  
  //__delay( 200 );
  //__writeMemory32(0x00418800, 0x50000000, "Memory");	//Address 0x50000001: 
                                                        //event thres[1:0] = 00 (PIR)
                                                        //PIR count[3:2] = 10 = divide by 24
                                                        //inhibit counter = 1000
                                                        //Address 0x50000002: 
                                                        //Trim memory clock divider[3:0] = 0001 = divide by 2
                                                        //Micro clock divider [5:4] = 00 - Full Speed
                                                        //Clock Selection [6] = 1 = XTAL

  //__message "***** SW reset! *****";
  //__writeMemory32(0x0000F700, 0x50000014, "Memory");	//SW uC reset[7] = enable           //CG: 0000F700
  
    // Wait for ms
  //__message "***** Waiting 1 sec. for the uC to reinit from SW reset! *****";    
  //__delay( 1000 );
  
  //__message "***** xtal, RC and PIR enabled! *****";
  //__writeMemory32(0x00007700, 0x50000014, "Memory");	//Same config as before  
  
  // Wait for ms
  __delay( 1000 );
  
  // Set emulator speed to 4MHz (Maximum for IAR at this moment)
  //__emulatorSpeed(4000000);                                                                 //CG: 4000000
  
  //__delay( 200 );
  
}


/*******************************************************************************
execUserPreload:
Called after communication with the target system is established
but before downloading the target application.
Implement this macro to initialize memory locations and/or
registers which are vital for loading data properly.
*******************************************************************************/
execUserPreload()
{

    __message " ***** execUserPreload() ***** ";
    __delay (1000);    
  
}


/*******************************************************************************
execUserReset:
Called each time the reset command is issued.
Implement this macro to set up and restore data.
*******************************************************************************/
execUserReset()
{
    __delay (1000);  
    // tell the user what we are doing
    __message " ***** execUserReset() ***** ";

}

/*******************************************************************************
execUserSetup:
Called once after the target application is downloaded.
Implement this macro to set up the memory map, breakpoints,
interrupts, register macro files, etc.
*******************************************************************************/
execUserSetup()
{
  __message " ***** execUserSetup() ***** ";
  
}


/*******************************************************************************
execUserFlashReset:
Called once after the flash loader is downloaded to RAM, but
before execution of the flash loader. This macro is only called when
you are programming flash, and it should only be used for flash
loader functionality.
*******************************************************************************/
execUserFlashReset()
{
    __delay (1000);  
    __message " ***** execUserFlashReset() ***** ";
}

/*******************************************************************************
execUserExit:
Called once when the debug session ends.
Implement this macro to save status data etc.
*******************************************************************************/
execUserExit()
{
    __delay (1000);  
    __message " ***** execUserExit ***** ";
  
}

/*******************************************************************************
execUserFlashExit:
Called once when the debug session ends.
Implement this macro to save status data etc. This macro is useful
for flash loader functionality.
*******************************************************************************/
execUserFlashExit()
{
    __delay (1000);  
    __emulatorSpeed(100000);   //Must be kept to avoid problems upon reset
    //__delay( 200 );
    __message " ***** execUserFlashExit ***** ";
}